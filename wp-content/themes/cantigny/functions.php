<?php
include 'alloy/alloy.php';
include 'includes/alloy-template.php';
include 'includes/cantigny-map.php';
include 'includes/events.php';
include 'includes/events-admin.php';


class Cantigny_Functions {

  public function __construct() {

    add_action( 'init', array( $this, 'load_init' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ) );
    add_action( 'after_setup_theme', array( $this, 'load_acf_fields' ) );

  }

  public function load_init() {

    // Add options pages.
    acf_add_options_page();
	
	  acf_add_options_sub_page(array(
		  'page_title' 	=> 'Program Policy',
		  'menu_title'	=> 'Program Policy',
		  'parent_slug'	=> 'edit.php?post_type=tribe_events',
	  ));

  }

  public function load_assets() {

	  Alloy::Asset('add', array(
		  'type'  =>  'css',
		  'handle'  =>  'fancybox.css',
		  'src' =>  get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css'
	  ) );

//    Alloy::Asset( 'add', array(
//      'type' => 'css',
//      'handle' => 'application.css'
//    ) );

    wp_deregister_script('jquery');

    Alloy::Asset( 'add', array(
      'type' => 'js',
      'handle' => 'jquery',
      'src' => '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'
    ) );

    Alloy::Asset( 'add', array(
      'type' => 'js',
      'handle' => 'application-vendor.js'
    ) );

    Alloy::Asset( 'add', array(
      'type' => 'js',
      'handle' => 'application.js'
    ) );

  }

  public function load_acf_fields() {

    $theme_dir = Alloy::Constant( 'theme_dir' );
    $config_files = scandir( $theme_dir . '/acf-fields');

    if( !$config_files || count($config_files) <= 2 ) {
      return;
    }

    foreach($config_files as $file) {
      if( $file == '.' || $file == '..' || $file == '.DS_Store' ) {
        continue;
      }

      include_once $theme_dir . '/acf-fields/' . $file;

    }


  }

}
new Cantigny_Functions;

function alloy_load_template( $data=array(), $view='' ) {
  Timber::render( $view, $data);
}

// GF method: http://www.gravityhelp.com/documentation/gravity-forms/extending-gravity-forms/hooks/filters/gform_init_scripts_footer/
add_filter( 'gform_init_scripts_footer', '__return_true' );

// solution to move remaining JS from https://bjornjohansen.no/load-gravity-forms-js-in-footer
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
  $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
  return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
  $content = ' }, false );';
  return $content;
}


// Need a little cache here to keep track of current weather for the park.
// We will create 2 entries in the wp_options table.
// A timestamp and the weather data.

$cur_timestamp = time();
$w_timestamp = get_option( 'w_timestamp', 0 );

$time_diff = $cur_timestamp - $w_timestamp;

if( !$w_timestamp || $time_diff >= 3600 ) {

  // Update the timestamp.
  update_option( 'w_timestamp', $cur_timestamp );

  // Call the API.
  $api_call = wp_remote_get( 'http://api.openweathermap.org/data/2.5/weather?zip=60187,us&APPID=6e38ea54eef30de471af1269878b379d' );;
  $request = json_decode( wp_remote_retrieve_body( $api_call ) );
  $temp_k = $request->main->temp;

  $temp = $temp_k - 273;
  $temp = $temp * 1.8;
  $temp = ceil($temp + 32);

  $weather_code = $request->weather[0]->icon;

  $weather_data = serialize( array(
    'temp' => $temp,
    'icon' => $weather_code
  ) );

  update_option( 'w_conditions', $weather_data );


}

function get_breadcrumb( $cur_page ) {

  $cur_page = get_post( $cur_page );

  $breadcrumb_links = array();

  $breadcrumb_links[get_bloginfo('url')] = 'Home';

  $ancestors = array_reverse( get_post_ancestors( $cur_page->ID ) );

  if( $ancestors ) {

    foreach( $ancestors as $ancestor ) {

      $ancestor = get_post( $ancestor );
      $breadcrumb_links[ get_permalink( $ancestor->ID ) ] = $ancestor->post_title;

    }

  }

  $breadcrumb_links[ get_permalink($cur_page->ID) ] = $cur_page->post_title;

  $breadcrumb_html = '';

  $i = 0;
  $total = count($breadcrumb_links);

  foreach( $breadcrumb_links as $link => $label ) {

    $i++;

    $breadcrumb_html .= '<a href="' . $link . '" class="';

    if( $cur_page->post_title == $label ) {
      $breadcrumb_html .= ' active';
    }

    $breadcrumb_html .= '">' . $label . '</a>';

    if( $i != $total ) {
      $breadcrumb_html .= ' <i class="fa fa-chevron-right"></i> ';
    }
  }

  return $breadcrumb_html;

}

function first_child_redirect( $post_id ) {

  $page_args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'post_parent' => $post_id,
    'order' => 'ASC',
    'orderby' => 'menu_order'
  );
  $child = get_posts( $page_args );

  if( $child ) {

    $child = end($child);
    wp_redirect( get_permalink( $child->ID ) ); exit;

  }

}

add_action('init', function() {
  $blocks = get_field( 'layout_blocks', 85 );
});

//<editor-fold desc="Shortcodes">
add_shortcode('heading', 'cant_heading');
function cant_heading( $atts, $content="" ) {
  return '<p class="heading">' . $content . '</p>';
}

add_shortcode('download', 'cant_download');
function cant_download( $atts, $content="" ) {

  $atts = shortcode_atts( array(
    'url' => null
  ), $atts, 'download' );

  return '<p><a href="' . $atts['url'] . '" class="button green download compact" target="_blank" rel="nofollow"><i class="fa fa-cloud-download"></i> ' . $content . '</a></p>';
}

add_shortcode('button', 'cant_button');
function cant_button( $atts, $content="" ) {

  $atts = shortcode_atts( array(
    'url' => null,
    'new_window'  =>  null
  ), $atts, 'download' );
  
  if ( $atts['new_window'] != '' && $atts['new_window'] != 'no' && $atts['new_window'] != 'false' ) {
  	$new_window = 'target="_blank"';
  } else {
  	$new_window = '';
  }

  return '<p><a href="' . $atts['url'] . '" ' . $new_window . ' class="button green compact">' . $content . '</a></p>';
}

add_shortcode('interactive-map', 'cant_interactive_map');
function cant_interactive_map( $atts ) {
  
  $data = array(
    'map_image' => get_field( 'int_map_image', 'option' ),
    'point_icon' => get_field( 'int_map_point', 'option' ),
    'points' => get_field( 'map_points', 'option' )
  );

  return Timber::compile( Alloy::Constant( 'theme_dir' ) . '/views/partials/interactive-map.twig', $data );

}
//</editor-fold>

/**
 *  Filters the `[audio]` shortcode
 *
 * @since 1.0.0
 *
 * @param string $html Audio shortcode HTML output.
 * @param array $atts Array of audio shortcode attributes.
 * @param string $audio Audio file.
 * @param int $post_id Post ID.
 * @param string $library Media library used for the audio shortcode.
 */
function cant_audio_shortcode_filter($html, $atts, $audio, $post_id, $library)
{

	$listen_wrap = '<span class="listen-wrap"><span class="listen">Listen</span> <a href="#" class="audio-trigger"><span class="fa-stack fa-large"><i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i><i class="fa fa-volume-up fa-stack-1x inverse" aria-hidden="true"></i><span class="sr-only">Play Audio</span></span></a></span>';

	$html = sprintf('<div class="cant-audio-player">%1$s%2$s</div>',
		$html,
		$listen_wrap
	);

	return $html;
}

//add_filter('wp_audio_shortcode', 'cant_audio_shortcode_filter', 0, 5);

add_image_size( 'photo_block', 750, 400, true );

add_action( 'admin_head', 'alloy_admin_styles' );
function alloy_admin_styles() {
  ?>
  <style>

  @media(min-width: 992px) {
    .post-type-page .acf-flexible-content .layout .acf-fc-layout-handle {
      display: flex;
      align-items: center;
      font-weight: bold;
      background: #23282D;
      font-size: 25px;
      color: #fff;
    }
    .post-type-page .acf-flexible-content .layout .acf-fc-layout-order {
      margin-right: 20px;
      color: #fff;
      background: #0073AA;
    }
    .post-type-page .acf-flexible-content .layout .acf-fc-layout-controlls .acf-icon.-collapse:hover {
      color: #72777c;
      background: #fff;
    }
    .post-type-page .acf-flexible-content .layout .acf-fc-layout-controlls {
      margin-top: 7px;
    }

    .post-type-page #post-body.columns-2 #postbox-container-1,
    .post-type-page .postbox-container {
      float: none;
      width: 100%;
    }

    .post-type-page #poststuff #post-body.columns-2 #side-sortables {
      display: flex;
      justify-content: space-between;
      width: 100%;
    }

    .post-type-page #poststuff #post-body.columns-2 #side-sortables .postbox {
      width: 49%;
    }

    .post-type-page #poststuff #post-body.columns-2 #side-sortables #submitdiv {
      order: 2;
    }

    .post-type-page #poststuff #post-body.columns-2 {
      margin-right: 0;
    }

    .acf-fc-popup {
      margin-left: -69px!important;
    }
    .acf-fc-popup:before {
      display: none;
    }

  }
  </style>
  <?php
}

//<editor-fold desc="TinyMCE">
/*  Add TinyMCE 3rd Row
======================================*/
// Callback function to insert 'styleselect' into the $buttons array
function mce_format_add( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'mce_format_add' );

/*  Add TinyMCE Format Dropdown Items
======================================*/
// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' =>  'Accent Color Heading',
			'selector'  =>  '*',
			'classes' =>  'heading',
			'wrapper' =>  'p',
		),
		array(
			'title' =>  'Grey Small Heading',
			'selector'  =>  '*',
			'classes' =>  'grey-sub-heading',
			'wrapper' =>  'p',
		),
		array(
			'title' =>  'Link Small Heading',
			'selector'  =>  '*',
			'classes' =>  'link-sub-heading',
			'wrapper' =>  'p',
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
//</editor-fold>

/*-----------------------------------------------------------------------------------*/
/* Remove Unwanted Admin Menu Items */
/*-----------------------------------------------------------------------------------*/

function remove_admin_menu_items() {
  $remove_menu_items = array(
    __('Comments'),
    __('Posts'),
    __('Appearance'),
//    __('Tools'),
//    __('Custom'),
//    __('Fields'),
  );
  global $menu;
  end ($menu);
  while (prev($menu)){
    $item = explode(' ',$menu[key($menu)][0]);
    if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
    unset($menu[key($menu)]);
  }
  }
}

add_action('admin_menu', 'remove_admin_menu_items');


function add_acf_ninja( $field ) {
	$ninja_form = $field;
	if ( function_exists( 'ninja_forms_display_form' ) && is_array( $ninja_form ) ) { // Ninja forms 2.x
		ninja_forms_display_form( $ninja_form['id'] );
	} elseif ( function_exists( 'ninja_forms_display_form' ) && is_object( $ninja_form ) ) { // Ninja forms 2.x
		foreach ( $ninja_form as $form ) {
			ninja_forms_display_form( $form['id'] );
		}
	} elseif ( class_exists( 'Ninja_Forms' ) && is_array( $ninja_form ) ) { // Ninja forms 3.x
		Ninja_Forms()->display( $ninja_form['id'] );
	} elseif ( class_exists( 'Ninja_Forms' ) && is_object( $ninja_form ) ) { // Ninja forms 3.x
		foreach ( $ninja_form as $form ) {
			Ninja_Forms()->display( $form['id'] );
		}
	}
}

/**
 * Remove the emojis!
 *
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function sm_switch($sm_title, $url ) {
	$sm_title = strtolower( $sm_title );
	$sm_title = preg_replace( '/\s+/', '', $sm_title );
	$sm_title = preg_replace( '/[^ \w]+/', '', $sm_title );
	switch ( $sm_title ) {
		case ( 'facebook' ) :
			$title  = 'Facebook';
			$icon   = 'facebook';
			break;
		case ( 'linkedin' ) :
			$title  = 'LinkedIn';
			$icon   = 'linkedin';
			break;
		case ( 'twitter' ) :
			$title  = 'Twitter';
			$icon   = 'twitter';
			break;
		case ( 'youtube' ) :
			$title  = 'YouTube';
			$icon   = 'youtube';
			break;
		case ( 'pinterest' ) :
			$title  = 'Pinterest';
			$icon   = 'pinterest-p';
			break;
		case ( 'instagram' ) :
			$title  = 'Instagram';
			$icon   = 'instagram';
			break;
	} ?>
	<div class="social-media">
		<?php if ( !empty($url ) ) : ?>
			<a href="<?php esc_html_e( $url ); ?>">
		<?php endif; ?>
			<i class="fa fa-<?php echo $icon; ?>"></i>
			<p><?php echo $title; ?></p>
			<p>Follow Us</p>
		<?php if ( !empty($url ) ) : ?>
			</a>
		<?php endif; ?>
	</div>
<?php
}

function sm_loop_count( $loop_count ) {
	$class = '';
	switch ( $loop_count ) {
		case 1 :
			$class = 'one';
			break;
		case 2 :
			$class = 'two';
			break;
		case 3 :
			$class = 'three';
			break;
		case 4 :
			$class = 'four';
			break;
		case 5 :
			$class = 'five';
			break;
	}

	echo $class;
}

function stylesheet_rotator() {
	$date = date( 'm-d' );

	if ( ( $date >= ('12-01') ) && ( $date <= ('02-29') ) ) {
		$style = 'winter';
	} elseif ( ( $date >= ('03-01') ) && ( $date <= ('05-31') ) ) {
		$style = 'spring';
	} elseif ( ( $date >= ('06-01') ) && ($date <= ('08-31' ) ) ) {
		$style = 'summer';
	} elseif ( ($date >= ('09-01') ) && ($date <= ('11-30' ) ) ) {
		$style = 'autumn';
	}
	/*
	WpeCommon::purge_memcached();
	WpeCommon::clear_maxcdn_cache();
	WpeCommon::purge_varnish_cache();  // refresh our own cache (after CDN purge, in case that needed to clear before we access new content)
	$this->purge_object_cache();
	*/
	$stylesheet = ( get_field( 'stylesheet_selector', 'option' ) ) ? get_field( 'stylesheet_selector', 'option' ) : '';

	if ( $stylesheet ) {
		$style = $stylesheet;
		echo $style;
	} else {
		echo $style;
	}

}

function cant_stylesheet_cache_clear() {
	$screen = get_current_screen();

	if ( $screen->base == 'toplevel_page_acf-options' ) {

	}

}

add_action( 'current_screen', 'cant_stylesheet_cache_clear' );

function predump($item) {
	echo"<pre>";var_dump($item);echo"</pre>";
}

// Remove Roles
remove_role('author');
remove_role('contributor');
remove_role('editor');
remove_role('wpseo_editor');
remove_role('wpseo_manager');
remove_role('subscriber');

// Remove metafields from Dashboard
function dashboard_revamp() {
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'tribe_dashboard_widget', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
	remove_meta_box( 'wpe_dify_news_feed', 'dashboard', 'normal' );
	
}

add_action( 'wp_dashboard_setup', 'dashboard_revamp' );

function cant_admin_menu() {
	$user = wp_get_current_user();
	if ( !in_array( 'administrator', (array) $user->roles ) && !in_array( 'client_admin', (array) $user->roles ) ) {
		remove_menu_page( 'tools.php' );                  //Tools
		remove_menu_page('acf-options');
	}
}

add_action( 'admin_init', 'cant_admin_menu' );

function remove_wp_nodes() {
	$user = wp_get_current_user();
	if ( !in_array( 'administrator', (array) $user->roles ) && !in_array( 'client_admin', (array) $user->roles ) ) {
		global $wp_admin_bar;
		 $wp_admin_bar->remove_node( 'new-post' );
		$wp_admin_bar->remove_node( 'new-link' );
		$wp_admin_bar->remove_node( 'new-user' );
		// $wp_admin_bar->remove_node( 'new-media' );
	}
	if ( in_array( 'calendar_editor', (array) $user->roles ) ) {
		global $wp_admin_bar;
		$wp_admin_bar->remove_node( 'new-page' );
	}
}

add_action( 'admin_bar_menu', 'remove_wp_nodes', 999 );


function cant_custom_login() {
	
	$date = date( 'm-d' );
	
	if ( ( $date >= ('12-01') ) && ( $date <= ('02-29') ) ) {
		$color = '#6caabb';
	} elseif ( ( $date >= ('03-01') ) && ( $date <= ('05-31') ) ) {
		$color = '#a55c92';
	} elseif ( ( $date >= ('06-01') ) && ($date <= ('08-31' ) ) ) {
		$color = '#217a44';
	} elseif ( ($date >= ('09-01') ) && ($date <= ('11-30' ) ) ) {
		$color = '#9a3428';
	}
	
	?>
	<style type="text/css">
		body { background-color: <?php echo $color; ?>!important; }
		body a {  color: #ffffff!important; }
		body a:hover { color: #00a0d2!important; }
		body.login div#login h1 { background: #ffffff; padding-top: 10px; margin-bottom: -5px; }
		body.login div#login h1 a {
			background-image: url('/wp-content/themes/cantigny/assets/img/login-logo.png');
			padding-top: 30px;
			padding-bottom: 30px;
			margin-bottom: 0;
			width: 320px;
			background-size: contain;
		}
		body.login form { margin-top: 0; }
	</style>
	<?php
} add_action( 'login_enqueue_scripts', 'cant_custom_login' );