<?php
/**
 * user.php
 *
 * @package Alloy
 * @subpackage User
 * @since 0.1.0
 */

/**
 * Manage a user in WordPress
 *
 * The User class allows you to get specific data about a user.
 *
 * @since 0.1.0
 */
class User {

  /**
   * Get the requested data for a user.
   * @param  array  $args A query and return array.
   * @return array       The returned fields.
   */
  public function get( $args=array() ) {

    $args = $this->get_user_args( $args );

    return $this->get_user_data( $args );

  }

  /**
   * Define some args to pass to Fetch.
   * @param  array  $args The data query array.
   * @return array        An array of args.
   */
  public function get_user_args( $args=array() ) {

    $args['query']['query_type'] = 'user';

    return $args;

  }

  /**
   * Get requested user data.
   * @param  array  $args The query and return parameters.
   * @return array        An array of data.
   */
  public function get_user_data( $args=array() ) {

    // Abort if required field isn't present.
    if( !$args['query']['ID'] ) {
      return;
    }

    // Get the user object.
    $wp_user_obj = get_userdata($args['query']['ID']);

    // Abort if user doesn't exist.
    if( !is_object($wp_user_obj) ) {
      return;
    }

    $presets = array(
      'ID',
      'user_nicename',
      'user_email',
      'user_url',
      'user_registered',
      'display_name',
      'first_name',
      'last_name',
      'nickname',
      'description'
    );

    $acf_id = 'user_' . $wp_user_obj->ID;

    $fetch_args = array(
      'return' => $args['return'],
      'wp_obj' => $wp_user_obj,
      'presets' => $presets,
      'acf_id' => $acf_id
    );

    return Alloy::Fetch( 'get_return_data', $fetch_args );

  }

}