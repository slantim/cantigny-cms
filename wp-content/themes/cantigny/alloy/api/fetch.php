<?php
/**
 * fetch.php
 *
 * @package Alloy
 * @subpackage Fetch
 * @since 0.1.0
 */

/**
 * Get data
 *
 * Fetch returns data for a specified type in the format requested.
 *
 * @since 0.1.0
 */
class Fetch {

  /**
   * Get WP fields, custom field groups, and custom fields.
   * @param  array  $args An array of arguments for the data request.
   * @return array                Returns the data.
   */
  // public function get_return_data( $query_return=array(), $wp_obj=array(), $presets=array(), $acf_id='' ) {
  public function get_return_data( $args=array() ) {

    // If $wp_obj is an array convert it to an object.
    if( is_object( $args['wp_obj'] ) ) {
      $args['wp_obj'] = array_to_object( $args['wp_obj'] );
    }

    $return_data = array();

    foreach( $args['return'] as $return ) {

      // Check to see if it's WP data.
      if( in_array($return, $args['presets'] ) ) {

        if( strstr( $args['acf_id'], 'user_' ) ) {

          $return_data[$return] = $args['wp_obj']->data->$return;

        } else {

          $return_data[$return] = $args['wp_obj']->$return;

        }

        continue;
      }

      // Check to see if it's a field group.
      $group_fields = Alloy::ACF('group_data', array(
        'title' => $return,
        'id' => $args['acf_id']
      ));

      if( $group_fields ) {
        $group_slug = sanitize_title( $return );
        $return_data[$group_slug] = $group_fields;
        continue;
      }

      // Check to see if it's a one off field.
      $field = get_field( $return, $args['acf_id'] );

      $return_data[$return] = $field;

    }

    return $return_data;
  }

}