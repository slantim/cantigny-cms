<?php
/**
 * term.php
 *
 * @package Alloy
 * @subpackage Term
 * @since 0.1.0
 */

/**
 * Manage a term
 *
 * Allows you to (for now) get data about a term.
 *
 * @since 0.1.0
 */
class Term {

  /**
   * Get data for a term.
   * @param  array  $args Args for the post in the Data Request Syntax
   * @return array       An array of term data.
   */
  public function get( $args=array() ) {

    $args = $this->get_term_args( $args );

    return $this->get_term_data( $args );

  }

  /**
   * Set up some default args for this.
   * @param  array  $args Args for the post in the Data Request Syntax
   * @return array       A modified array of args.
   */
  public function get_term_args( $args=array() ) {

    $args['query']['query_type'] = 'term';

    return $args;

  }

  /**
   * Get requested term data.
   * @param  array  $args The query and return parameters.
   * @return array        An array of data.
   */
  public function get_term_data( $args=array() ) {

    // Abort if required fields aren't present.
    if( !$args['query']['field'] || !$args['query']['value'] ) {
      return;
    }

    // Get the term object.
    $wp_term_obj = get_term_by( $args['query']['field'], $args['query']['value'], $args['query']['taxonomy'], $args['query']['output'], $args['query']['filter'] );

    $presets = array(
      'term_id',
      'name',
      'slug',
      'term_group',
      'term_taxonomy_id',
      'taxonomy',
      'description',
      'parent',
      'count'
    );

    $acf_id = $wp_term_obj;

    $fetch_args = array(
      'return' => $args['return'],
      'wp_obj' => $wp_term_obj,
      'presets' => $presets,
      'acf_id' => $acf_id
    );

    return Alloy::Fetch( 'get_return_data', $fetch_args );

  }

}