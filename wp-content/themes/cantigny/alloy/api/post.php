<?php
/**
 * post.php
 *
 * @package Alloy
 * @subpackage Post
 * @since 0.1.0
 */

/**
 * Manage a post
 *
 * Allows you to (for now) get data about a post.
 *
 * @since 0.1.0
 */
class Post {

  /**
   * Get data for a post.
   * @param  array  $args Args for the post in the Data Request Syntax
   * @return array       An array of post data.
   */
  public function get( $args=array() ) {

    $args = $this->get_post_args( $args );

    return $this->get_post_data( $args );

  }

  /**
   * Set up some default args for this.
   * @param  array  $args Args for the post in the Data Request Syntax
   * @return array       A modified array of args.
   */
  public function get_post_args( $args=array() ) {

    $args['query']['query_type'] = 'post';

    return $args;

  }

  /**
   * Get requested post data.
   * @param  array  $args The query and return parameters.
   * @return array        An array of data.
   */
  public function get_post_data( $args=array() ) {

    // Abort if required fields aren't present.
    if( !$args['query']['ID'] ) {
      return;
    }

    // Get the term object.
    $wp_post_obj = get_post( $args['query']['ID'] );

    $presets = array(
      'ID',
      'post_author',
      'post_name',
      'post_type',
      'post_title',
      'post_date',
      'post_date_gmt',
      'post_content',
      'post_excerpt',
      'post_status',
      'comment_status',
      'ping_status',
      'post_password',
      'post_parent',
      'post_modified',
      'post_modified_gmt',
      'comment_count',
      'menu_order'
    );

    $acf_id = $args['query']['ID'];

    $fetch_args = array(
      'return' => $args['return'],
      'wp_obj' => $wp_post_obj,
      'presets' => $presets,
      'acf_id' => $acf_id
    );

    return Alloy::Fetch( 'get_return_data', $fetch_args );

  }

}