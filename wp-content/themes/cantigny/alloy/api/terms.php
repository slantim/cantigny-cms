<?php
/**
 * terms.php
 *
 * @package Alloy
 * @subpackage Terms
 * @since 0.1.0
 */

/**
 * Manage terms
 *
 * Allows you to (for now) get data about terms.
 *
 * @since 0.1.0
 */
class Terms {

  /**
   * Get a list of terms.
   * @param  array  $args Args for the terms in the Data Request Syntax
   * @return array       An array of terms.
   */
  public function get( $args=array() ) {

    $args = $this->get_terms_args( $args );

    return $this->get_terms_data( $args );

  }

  /**
   * Set up some default args for this.
   * @param  array  $args Args for the terms in the Data Request Syntax
   * @return array       A modified array of args.
   */
  public function get_terms_args( $args=array() ) {

    $args['query']['query_type'] = 'terms';

    if( !$args['query']['fields'] ) {
      $args['query']['fields'] = 'ids';
    }

    return $args;

  }

  /**
   * Get requested terms data.
   * @param  array  $args The query and return parameters.
   * @return array        An array of data.
   */
  public function get_terms_data( $args=array() ) {

    // Abort if required fields aren't present.
    if( !$args['query']['taxonomy'] ) {
      return;
    }

    $terms = get_terms( $args['query'] );

    if( !$terms ) {
      return;
    }

    $term_data = array();

    foreach( $terms as $term ) {

      $term_data[] = Alloy::Term('get', array(
        'query' => array(
          'field' => 'id',
          'value' => $term,
          'taxonomy' => $args['query']['taxonomy']
        ),
        'return' => $args['return']
      ));

    }

    return $term_data;

  }

}