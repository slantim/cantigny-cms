<?php
/**
 * posts.php
 *
 * @package Alloy
 * @subpackage Posts
 * @since 0.1.0
 */

/**
 * Manage posts
 *
 * Allows you to (for now) get data about posts.
 *
 * @since 0.1.0
 */
class Posts {

  /**
   * Get a list of posts.
   * @param  array  $args Args for the posts in the Data Request Syntax
   * @return array       An array of posts.
   */
  public function get( $args=array() ) {

    $args = $this->get_posts_args( $args );

    return $this->get_posts_data( $args );

  }

  /**
   * Set up some default args for this.
   * @param  array  $args Args for the terms in the Data Request Syntax
   * @return array       A modified array of args.
   */
  public function get_posts_args( $args=array() ) {

    $args['query']['query_type'] = 'posts';

    if( !$args['query']['fields'] ) {
      $args['query']['fields'] = 'ids';
    }

    return $args;

  }

  /**
   * Get requested posts data.
   * @param  array  $args The query and return parameters.
   * @return array        An array of data.
   */
  public function get_posts_data( $args=array() ) {

    // Abort if required fields aren't present.
    if( !$args['query']['post_type'] ) {
      return;
    }

    $posts = get_posts( $args['query'] );

    if( !$posts ) {
      return;
    }

    $post_data = array();

    // We want to return fields not just IDs.
    unset( $args['query']['fields'] );

    foreach( $posts as $post ) {

      $args['query']['ID'] = $post;

      $post_data[] = Alloy::Post('get', array(
        'query' => $args['query'],
        'return' => $args['return']
      ));

    }

    return $post_data;

  }

}