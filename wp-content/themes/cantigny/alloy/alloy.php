<?php
/**
 * Main Alloy file
 *
 * This file exposes the Alloy API to the entire WordPress theme.
 *
 * @package Alloy
 * @since 0.1.0
 */

// Get configs.
include 'config/constants.config.php';

// Load the API.
include 'functions.php';
include 'api/bootstrap.php';
include 'api/constants.php';
include 'api/post-type.php';
include 'api/taxonomy.php';
include 'api/asset.php';
include 'api/fetch.php';
include 'api/user.php';
include 'api/acf.php';
include 'api/fieldset.php';
include 'api/terms.php';
include 'api/term.php';
include 'api/post.php';
include 'api/posts.php';

/**
 * The Alloy class.
 *
 * This class provides an interfact to the API. It's methods are available statically
 * to any theme file as long as functions.php is including alloy.php.
 *
 * @since 0.1.0
 */
class Alloy {

  /**
   * Expose the Constants class to Alloy.
   * @param string $constant The key to access the constant being requested.
   */
  public function Constant( $constant ) {
    $constants = new Constants;
    return $constants->get($constant);
  }

  /**
   * Expose the Post_Type class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Post_Type( $action='add', $args=array() ) {
    $post_type = new Post_Type;
    $post_type->$action($args);
  }

  /**
   * Expose the Taxonomy class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Taxonomy( $action='add', $args=array() ) {
    $taxonomy = new Taxonomy;
    $taxonomy->$action($args);
  }

  /**
   * Expose the Asset class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Asset( $action='add', $args=array() ) {
    $asset = new Asset;
    $asset->$action($args);
  }

  /**
   * Expose the User class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function User( $action='add', $args=array() ) {
    $user = new User;
    return $user->$action($args);
  }

  /**
   * Expose the Fetch class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Fetch( $action='add', $args=array() ) {
    $fetch = new Fetch;
    return $fetch->$action($args);
  }

  /**
   * Expose the ACF class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function ACF( $action='', $args=array() ) {
    $acf = new Alloy_ACF;
    return $acf->$action($args);
  }

  /**
   * Expose the Layout part of the Fieldset class to the Alloy class.
   * @param string $label The field label
   * @param string $name  The field name
   * @param array  $args  ACF args for the flexible content field.
   */
  public function Layout( $label='', $name='', $args=array() ) {
    $fieldset = new Fieldset;
    return $fieldset->register_layout( $label, $name, $args );
  }

  /**
   * Expose the Fieldset class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Fieldset( $action='', $args=array() ) {
    $fieldset = new Fieldset;
    return $fieldset->$action($args);
  }

  /**
   * Expose the Field part of the Fieldset class to the Alloy class.
   * @param string $type  The type of field being registered.
   * @param string $label The field label
   * @param string $name  The field name
   * @param array  $args  ACF args for the field.
   */
  public function Field( $type='text', $label='', $name='', $args=array() ) {
    $fieldset = new Fieldset;
    return $fieldset->register_field( $type, $label, $name, $args );
  }

  /**
   * Expose the Term class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Term( $action='get', $args=array() ) {
    $term = new Term;
    return $term->$action($args);
  }

  /**
   * Expose the Terms class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Terms( $action='get', $args=array() ) {
    $terms = new Terms;
    return $terms->$action($args);
  }

  /**
   * Expose the Post class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Post( $action='get', $args=array() ) {
    $post = new Post;
    return $post->$action($args);
  }

  /**
   * Expose the Posts class to the Alloy class.
   * @param string $action The action this method will be performing.
   * @param array  $args   Developer defined arguments.
   */
  public function Posts( $action='get', $args=array() ) {
    $posts = new Posts;
    return $posts->$action($args);
  }

}