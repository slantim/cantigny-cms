<?php
function array_to_object( $array=array() ) {

  $object = json_decode(json_encode($array), FALSE);
  return $object;

}

add_action('after_setup_theme', 'alloy_theme_support');
function alloy_theme_support() {

    /**
     * Register theme support for post thumbnails.
     */
    add_theme_support( 'post-thumbnails' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

}

function alloy_get_fields( $key='', $fields=array() ) {

    $field_data = array();

    foreach( $fields as $field ) {

        $field_data[$field] = get_field( $field, $key );

    }

    return $field_data;

}

function alloy_create_slug( $string=null ) {
    return str_replace( '-', '_', sanitize_title( $string ) );
}


// Hide if Empty: Hide elements if they have no content.
add_action('wp_head', 'hide_if_empty');
function hide_if_empty() {
  echo '<style>.hie:empty { display: none!important; }</style>';
}
