<?php

define( 'alloy_site_title', get_bloginfo('name') );
define( 'alloy_site_url', get_bloginfo('url') );
define( 'alloy_theme_url', get_template_directory_uri() );
define( 'alloy_theme_dir', get_template_directory() );
define( 'alloy_assets_url', alloy_theme_url . '/assets' );
define( 'alloy_css_url', alloy_assets_url . '/css' );
define( 'alloy_js_url', alloy_assets_url . '/js' );
define( 'alloy_media_url', alloy_assets_url . '/resources' );

// Save the ACF field groups as a constant.
$acf_field_groups = acf_get_field_groups();

if( $acf_field_groups ) {
  // define('alloy_acf_groups', $acf_field_groups);
}