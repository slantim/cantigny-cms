<?php

/**
 *
 * This creates a new repeater field inside the Event Metabox in Single Events
 * This unsets the "Additional Functionality" meta content that is not relevant
 * to this web builds needs.
 *
 * Since we are hooking into a TribeMetaBox, we don't need to know whether or not the
 * `post_type` is `tribe_events`. Under normal conditions, we would need to declare what
 * `post_type` we need these fields to appear on.
 *
 */

if(!function_exists('create_cost_repeater_field')) {
	$tec = TribeEvents::instance();
	remove_action( 'tribe_events_cost_table', array($tec, 'maybeShowMetaUpsell'));
	add_action( 'tribe_events_cost_table', 'create_cost_repeater_field', 10);
	function create_cost_repeater_field() {
		if(class_exists( 'TribeEventsPro' )){ ?>
			<?php
			// This script allows for us to add new rows of the fieldset
			?>
			<script type="text/javascript">
				jQuery(document).ready(function( $ ){
					$( '#add-row' ).on('click', function() {
						var row = $( '.empty-row.screen-reader-text' ).clone(true);
						row.removeClass( 'empty-row screen-reader-text' );
						row.insertBefore( '.repeatable-fieldset-one >tr:last' );
						return false;
					});

					$( '.remove-row' ).on('click', function() {
						$(this).parents('tr').remove();
						return false;
					});
				});
			</script>
			<?php
			// This is just some styling to adjust the layout to be more fluid and consistent
			// with the rest of the meta box
			?>
			<style>
				#event_cost > tbody > tr { display: none; }
				#event_cost .eventBritePluginPlug, #event_cost .repeatable-fieldset-one tr, #event_cost .add-more-rows tr { display: block;}
				#event_cost .tribe_sectionheader { display: block; width: 100%; }
			</style>
<?php
			// Start grabbing the ID's needed for the post
			global $post;
			$post_id = $post->ID;

			// Create a new post_meta
			$value = get_post_meta($post_id, 'repeatable_fields', true);
			// Register the new post_meta
			wp_nonce_field('tribe_additional_costs', 'tribe_additional_costs' ); ?>
			<tr class="eventBritePluginPlug">
				<td colspan="4" class="tribe_sectionheader">
					<h4><?php _e('Event Cost', 'avia_framework'); ?></h4>
				</td>
			</tr>
			<?php
			// Start creating the fields
			?>
			<tbody class="repeatable-fieldset-one">
<?php
			if ( $value ) :
				// If there are currently fields populated with content, then keep those fields on the page
				// and display their current values
				foreach ( $value as $v ) : ?>
				<tr>
					<td width="30%">
						<label for="EventItemLabel"><b><?php _e('Cost Label: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventItemLabel'
							name='item_label[]'
							value='<?php if ( $v['item_label'] != '' ) : echo esc_attr( $v['item_label']); endif;?>'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="30%">
						<label for="EventAdditionalCost"><b><?php _e('Cost: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventAdditionalCost'
							name='add_cost[]'
							value='<?php if ( $v['add_cost'] != '' ) : echo esc_attr( $v['add_cost']); endif;?>'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="30%">
						<label for="EventItemMultiplier"><b><?php _e('Cost Multiplier: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventItemMultiplier'
							name='item_multiplier[]'
							value='<?php if ( $v['item_multiplier'] != '' ) : echo esc_attr( $v['item_multiplier']); endif;?>'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="10%">
						<label>&nbsp;</label><br />
						<a class="button remove-row" href="#" title="Remove">X</a></td>
				</tr>
<?php
				endforeach;
			// Otherwise, let's show some empty fields that we can add content to.
			else : ?>
				<tr>
					<td width="30%">
						<label for="EventItemLabel"><b><?php _e('Cost Label: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventItemLabel'
							name='item_label[]'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="30%">
						<label for="EventAdditionalCost"><b><?php _e('Cost: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventAdditionalCost'
							name='add_cost[]'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="30%">
						<label for="EventItemMultiplier"><b><?php _e('Cost Multiplier: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventItemMultiplier'
							name='item_multiplier[]'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="10%">
						<label>&nbsp;</label><br />
						<a class="button remove-row" href="#" title="Remove">X</a></td>
				</tr>
<?php
			endif;
			// This is used to "clone" the fields when we need to add more, but we only want to clone empty fields,
			// so we copy the content within the `else` statement. The only change here is that we wrap the `tr`
			// in a `screen-reader-text` class so that it's hidden from physical view
			?>
				<tr class="empty-row screen-reader-text">
					<td width="30%">
						<label for="EventItemLabel"><b><?php _e('Cost Label: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventItemLabel'
							name='item_label[]'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="30%">
						<label for="EventAdditionalCost"><b><?php _e('Cost: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventAdditionalCost'
							name='add_cost[]'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="30%">
						<label for="EventItemMultiplier"><b><?php _e('Cost Multiplier: ', 'the-event-calendar'); ?></b></label><br />
						<input
							type='text'
							id='EventItemMultiplier'
							name='item_multiplier[]'
							class=''
							autocomplete="off"
						/>
					</td>
					<td width="10%">
						<label>&nbsp;</label><br />
						<a class="button remove-row" href="#" title="Remove">X</a></td>
				</tr>
			</tbody>
			<tbody class="add-more-rows">
				<tr>
					<td>
						<p><a id="add-row" class="button" href="#" title="Add">+</a></p>
					</td>
				</tr>
			</tbody>
<?php
		}
	}
}


/**
 * Adds a checkbox in the Publish Metabox
 */
function tribe_events_metafield_program_policy() {
	
	$post_id = get_the_ID();
	
	if ( get_post_type($post_id) != 'tribe_events' ) {
		return;
	}
	
	$value = get_post_meta($post_id, '_add_program_policy', true);
	wp_nonce_field('program_policy_' . $post_id, 'program_policy' );
	
	?>
	<div class="misc-pub-section misc-pub-section-last">
		<label><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="_add_program_policy" />Add Program Policy Content</label><br/>
		<label><a href="/wp-admin/edit.php?post_type=tribe_events&page=acf-options-program-policy">Edit Program Policy</a></label>
	</div>
	<?php
}

add_action('post_submitbox_misc_actions', 'tribe_events_metafield_program_policy');

/**
 *
 * This function will allow us to save the values in the repeater field to the database in the post_meta
 * which we can then call to on the front end of the site. We need to pass the $post_id to the function to
 * properly save the fields
 *
 * @param $post_id
 *
 */
function cant_save_cost_repeater_field($post_id) {

	if (
		!isset($_POST['tribe_additional_costs']) ||
		!isset($_POST['program_policy']) ||
		!wp_verify_nonce($_POST['tribe_additional_costs'], 'tribe_additional_costs') ||
		!wp_verify_nonce($_POST['program_policy'], 'program_policy_'.$post_id)
	) {
		return;
	}

	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return;
	}

	if (!current_user_can('edit_post', $post_id)) {
		return;
	}

	// Let's check the fields before we save, this will come in handy at the end
	$old = get_post_meta( $post_id, 'repeatable_fields', true);
	$new = array();

	// Define the post fields
	$label = $_POST['item_label'];
	$cost  = $_POST['add_cost'];
	$multiplier = $_POST['item_multiplier'];

	$count = count($label);

	// For each fieldset, save the values removing unneeded html and slashes
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $label[$i] != '' ) {
			$new[$i]['item_label'] = stripslashes( strip_tags( $label[$i]) );
		}

		if ( $cost[$i] != '' ) {
			$new[$i]['add_cost'] = stripslashes( strip_tags( $cost[$i] ) );
		}

		if ( $multiplier[$i] != '' ) {
			$new[$i]['item_multiplier'] = stripslashes( strip_tags( $multiplier[$i] ) );
		}
	}

	// Check to see if the field had old data that's being overwritten. If it is being overwritten
	// then let's save the new data, otherwise this is a new field.
	if ( !empty($new) && $new != $old ) {
		update_post_meta( $post_id, 'repeatable_fields', $new);
	} elseif ( empty($new) && $old ) {
		delete_post_meta( $post_id, 'repeatable_fields', $new);
	}
	
	if (isset($_POST['_add_program_policy'])) {
		update_post_meta($post_id, '_add_program_policy', $_POST['_add_program_policy']);
	} else {
		delete_post_meta($post_id, '_add_program_policy');
	}

}
add_action('save_post', 'cant_save_cost_repeater_field');