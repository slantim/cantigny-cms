<?php
add_shortcode('cantigny-map', function() {
	
	$map_api  = get_field( 'map_api_key', 'option' );
	$map_lat  = get_field( 'map_lat', 'option' );
	$map_long = get_field( 'map_long', 'option' );
	
	$output = '<style> .labels { margin-top: -75px!important; color: #406618; font-weight: bold; font-size: 14px; } #map { height: 450px; width: 100%; } </style>';
	
	$output .= '<div id="map"></div>';
	$output .= '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=' . $map_api . '"></script>';
	$output .= '<script type="text/javascript" src="' . get_template_directory_uri() . '/assets/js/markerwithlabel.js"></script>';
	
	$output .= '<script type="text/javascript">
      // When the window has finished loading create our google map below
      google.maps.event.addDomListener(window, \'load\', init);

      function init() {
          // Basic options for a simple Google Map
          // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions

          var mapOptions = {
              // How zoomed in you want the map to start at (always required)
              zoom: 14,

              // The latitude and longitude to center the map (always required)
              center: new google.maps.LatLng(' . $map_lat . ', ' . $map_long . '),

              // How you would like to style the map.
              // This is where you would paste any style found on Snazzy Maps.
              styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative.country","elementType":"labels.text.stroke","stylers":[{"color":"#c72033"},{"weight":"5"}]},{"featureType":"administrative.province","elementType":"labels.text.fill","stylers":[{"hue":"#ff0000"},{"lightness":"100"},{"weight":"5"},{"visibility":"on"}]},{"featureType":"administrative.province","elementType":"labels.text.stroke","stylers":[{"color":"#c72033"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#c72033"}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":"7"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#f8f8f8"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#dddddd"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#c72033"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#c72033"},{"weight":"1"},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#ff0000"},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"hue":"#ff0000"},{"lightness":"100"},{"saturation":"-100"},{"gamma":"0.00"},{"weight":"0.01"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"color":"#c72033"},{"visibility":"on"},{"weight":"5"}]},{"featureType":"road.highway.controlled_access","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#c72033"}]},{"featureType":"road.arterial","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#8b8b8b"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#e5e5e5"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#8b8b8b"}]}]
          };

          // Get the HTML DOM element that will contain your map
          // We are using a div with id="map" seen below in the <body>
          var mapElement = document.getElementById(\'map\');

          // Create the Google Map using our element and options defined above
          var map = new google.maps.Map(mapElement, mapOptions);

          var marker = new MarkerWithLabel({
              position: new google.maps.LatLng(' . $map_lat . ', ' . $map_long . '),
              map: map,
              draggable: true,
              raiseOnDrag: true,
              labelContent: "Cantigny Park",
              labelAnchor: new google.maps.Point(15, 65),
              labelClass: "labels", // the CSS class for the label
              labelInBackground: false,
              icon: "' . get_template_directory_uri() . '/assets/resources/marker.png"
          });

          var marker2 = new MarkerWithLabel({
              position: new google.maps.LatLng(41.847125, -88.155267),
              map: map,
              draggable: true,
              raiseOnDrag: true,
              labelContent: "Cantigny Golf",
              labelAnchor: new google.maps.Point(15, 65),
              labelClass: "labels", // the CSS class for the label
              labelInBackground: false,
              icon: "' . get_template_directory_uri() . '/assets/resources/marker.png"
          });
          
		$(\'#map-block\').on(\'click\', function(e) {
			//Set Center
			map.setCenter(new google.maps.LatLng(' . $map_lat . ', ' . $map_long . '));
		});
      
      }
      
      
   
  </script>';

  return $output;

});