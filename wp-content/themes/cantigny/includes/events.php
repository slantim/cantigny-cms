<?php

add_action('tribe_events_bar_after_template', 'events_category_bar');
function events_category_bar() {

  $query = get_queried_object();

  if( !$query || !$query->taxonomy ) {
    $data['all_events'] = true;
  }

  $terms = get_terms( array(
    'hide_empty' => false,
    'taxonomy' => 'tribe_events_cat',
    'order' => 'ASC'
  ) );

  if( !$terms ) {
    return;
  }

  $term_data = array();

  foreach( $terms as $term ) {

    $term_link = get_term_link( $term );

    if( is_wp_error( $term_link ) ) {
      continue;
    }

    if( $query->term_id == $term->term_id ) {
      $active = true;
    } else {
      $active = false;
    }

    $cat_icon = get_field( 'cat_icon', $term );
    $cat_color = get_field( 'cat_color', $term );

    $term_data[] = array(
      'permalink' => $term_link,
      'title' => $term->name,
      'icon' => $cat_icon,
      'color' => $cat_color,
      'active' => $active
    );

  }

  $data['term_data'] = $term_data;

  Timber::render( Alloy::Constant('theme_dir') . '/views/partials/event-categories.twig', $data);

}

function get_event_data( $featured_events=array() ) {

  if( !$featured_events ) {
    return;
  }

  $event_data = array();

  foreach( $featured_events as $event ) {

    $event = $event['event'];

    $event_data[] = get_single_event( $event->ID, $event );

  }

  return $event_data;

}

function get_single_event( $event_id, $event=null ) {

  if( !$event ) {
    $event = get_post( $event_id );
  }

  $date = date( 'l, M d', strtotime( get_post_meta( $event_id, '_EventStartDate', true ) ) );
  $start_time = date( 'h:i a', strtotime( get_post_meta( $event_id, '_EventStartDate', true ) ) );
  $end_date = date( 'l, M d', strtotime( get_post_meta( $event_id, '_EventEndDate', true ) ) );
  $end_time = date( 'h:i a', strtotime( get_post_meta( $event_id, '_EventEndDate', true ) ) );

  $permalink = get_permalink( $event_id );
  $title = $event->post_title;

  // Get venue
  $venue_id = get_post_meta( $event_id, '_EventVenueID', true );

  $venue_post = get_post( $venue_id );

  $venue = array(
    'title' => $venue_post->post_title,
    'address' => get_post_meta( $venue_id, '_VenueAddress', true ),
    'city' => get_post_meta( $venue_id, '_VenueCity', true ),
    'state' => get_post_meta( $venue_id, '_VenueState', true ),
    'zip' => get_post_meta( $venue_id, '_VenueZip', true ),
  );

  // Get cost.
  $cost = array(
    'symbol' => get_post_meta( $event_id, '_EventCurrencySymbol', true ),
    'cost' => get_post_meta( $event_id, '_EventCost', true ),
  );

  if( has_post_thumbnail( $event_id ) ) {
    $thumbnail = wp_get_attachment_url( get_post_thumbnail_id( $event_id ) );
  } else {
    $thumbnail = null;
  }

  $category = wp_get_post_terms( $event_id, 'tribe_events_cat' );
  $category = end( $category );

  $cat_link = get_term_link( $category );
  $cat_color = get_field( 'cat_color', $category );
  $cat_icon = get_field( 'cat_icon', $category );

  $event_data = array(
    'date' => $date,
    'start_date' => $date,
    'start_time' => $start_time,
    'end_date' => $end_date,
    'end_time' => $end_time,
    'permalink' => $permalink,
    'title' => $title,
    'thumbnail' => $thumbnail,
    'category' => $category->name,
    'cat_color' => $cat_color,
    'cat_icon' => $cat_icon,
    'cat_link' => $cat_link,
    'cat_slug' => $category->slug,
    'venue' => $venue,
    'cost' => $cost
  );

  return $event_data;
}

/*
 * Alters event's archive titles
 */
function tribe_alter_event_archive_titles ( $original_recipe_title, $depth ) {

	// Modify the titles here
	// Some of these include %1$s and %2$s, these will be replaced with relevant dates
	$title_upcoming =   'Upcoming Events'; // List View: Upcoming events
	$title_past =       'Past Events'; // List view: Past events
	$title_range =      'Events for %1$s - %2$s'; // List view: range of dates being viewed
	$title_month =      'Events for %1$s'; // Month View, %1$s = the name of the month
	$title_day =        'Events for %1$s'; // Day View, %1$s = the day
	$title_all =        'All events for %s'; // showing all recurrences of an event, %s = event title
	$title_week =       'Events for week of %s'; // Week view

	// Don't modify anything below this unless you know what it does
	global $wp_query;
	$tribe_ecp = Tribe__Events__Main::instance();
	$date_format = apply_filters( 'tribe_events_pro_page_title_date_format', tribe_get_date_format( true ) );

	// Default Title
	$title = $title_upcoming;

	// If there's a date selected in the tribe bar, show the date range of the currently showing events
	if ( isset( $_REQUEST['tribe-bar-date'] ) && $wp_query->have_posts() ) {

		if ( $wp_query->get( 'paged' ) > 1 ) {
			// if we're on page 1, show the selected tribe-bar-date as the first date in the range
			$first_event_date = tribe_get_start_date( $wp_query->posts[0], false );
		} else {
			//otherwise show the start date of the first event in the results
			$first_event_date = tribe_event_format_date( $_REQUEST['tribe-bar-date'], false );
		}

		$last_event_date = tribe_get_end_date( $wp_query->posts[ count( $wp_query->posts ) - 1 ], false );
		$title = sprintf( $title_range, $first_event_date, $last_event_date );
	} elseif ( tribe_is_past() ) {
		$title = $title_past;
	}

	// Month view title
	if ( tribe_is_month() ) {
		$title = sprintf(
			$title_month,
			date_i18n( tribe_get_option( 'monthAndYearFormat', 'F Y' ), strtotime( tribe_get_month_view_date() ) )
		);
	}

	// Day view title
	if ( tribe_is_day() ) {
		$title = sprintf(
			$title_day,
			date_i18n( tribe_get_date_format( true ), strtotime( $wp_query->get( 'start_date' ) ) )
		);
	}

	// All recurrences of an event
	if ( function_exists('tribe_is_showing_all') && tribe_is_showing_all() ) {
		$title = sprintf( $title_all, get_the_title() );
	}

	// Week view title
	if ( function_exists('tribe_is_week') && tribe_is_week() ) {
		$title = sprintf(
			$title_week,
			date_i18n( $date_format, strtotime( tribe_get_first_week_day( $wp_query->get( 'start_date' ) ) ) )
		);
	}

	if ( is_tax( $tribe_ecp->get_event_taxonomy() ) && $depth ) {
		$cat = get_queried_object();
		$title = $title . ' &#8250; ' . $cat->name;
	}

	return $title;
}
add_filter( 'tribe_get_events_title', 'tribe_alter_event_archive_titles', 11, 2 );

/**
 *  Displays link to previous month on Event Calendar month view
 *
 *  @since 1.0.0
 *
 *  @param string Formatted link
 *
 *  @return strinf Formatted html for the link
 *
 */
function cant_tribe_events_the_previous_month_link( $html ){

	$url  = tribe_get_previous_month_link();
	$date = Tribe__Events__Main::instance()->previousMonth( tribe_get_month_view_date() );
	$earliest_event_date = tribe_events_earliest_date( Tribe__Date_Utils::DBYEARMONTHTIMEFORMAT );

	// Only form the link if a) we have a known earliest event date and b) the previous month date is the same or later
	if ( $earliest_event_date && $date >= $earliest_event_date ) :
		$text = tribe_get_previous_month_text();

		$html = sprintf( '<a data-month="%1$s" href="%2$s" rel="prev">%4$s<span class="month-name">%3$s</span></a>',
			$date,
			esc_url( $url ),
			$text,
			'<span class="fa fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-long-arrow-left fa-stack-1x fa-inverse" aria-hidden="true"></i></span>'
		);

	endif;

	return $html;
}
add_filter( 'tribe_events_the_previous_month_link', 'cant_tribe_events_the_previous_month_link' );


/**
 *  Displays link to next month on Event Calendar month view
 *
 *  @since 1.0.0
 *
 *  @param string Formatted link
 *
 *  @return strinf Formatted html for the link
 */
function cant_tribe_events_the_next_month_link( $html ){

	$url  = tribe_get_next_month_link();
	$text = tribe_get_next_month_text();

	// Only form the link if a) we have a known earliest event date and b) the previous month date is the same or later
	if ( ! empty( $url ) ) :

		$date = Tribe__Events__Main::instance()->nextMonth( tribe_get_month_view_date() );

		if ( $date <= tribe_events_latest_date( Tribe__Date_Utils::DBYEARMONTHTIMEFORMAT ) ) {
			$html = sprintf( '<a data-month="%1$s" href="%2$s" rel="next"><span class="month-name">%3$s</span>%4$s</a>',
				$date,
				esc_url( $url ),
				$text,
				'<span class="fa fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-long-arrow-right fa-stack-1x fa-inverse" aria-hidden="true"></i></span>'
			);
		}



	endif;

	return $html;
}
add_filter( 'tribe_events_the_next_month_link', 'cant_tribe_events_the_next_month_link' );

/**
 *  Filters the days of the week that appear on the Event Calendar
 *
 *  @since 1.0.0
 *
 *  @param array $days_of_week The days of the week
 *
 *  @return array $$days_of_week Filtered days
 */
function cant_tribe_events_get_days_of_week( $days_of_week = array() ){

	$days = [];
	$days['Monday']    = 'Mon';
	$days['Tuesday']   = 'Tue';
	$days['Wednesday'] = 'Wed';
	$days['Thursday']  = 'Thu';
	$days['Friday']    = 'Fri';
	$days['Saturday']  = 'Sat';
	$days['Sunday']    = 'Sun';

	$new_days = array_merge(
		array_flip( $days_of_week ),
		$days
	);

	return $new_days;
}
add_filter( 'tribe_events_get_days_of_week', 'cant_tribe_events_get_days_of_week' );

/*
 * Change the label and placeholder texts in Events Search Bar
 */
add_filter('tribe-events-bar-filters', 'modify_searchbar_label', 100);

function modify_searchbar_label(array $filters)
{

	if (!isset($filters['tribe-bar-search'])) return $filters;
	$filters['tribe-bar-search']['caption'] = esc_attr__('Looking for something?', 'the-events-calendar');;
	$filters['tribe-bar-search']['html'] = str_replace('Search', 'Search', $filters['tribe-bar-search']['html']);

	return $filters;
}

/**
 * Redirect event category requests to list view.
 *
 * @param $query
 */
function use_list_view_for_categories( $query ) {
	// Disregard anything except a main archive query
	if ( is_admin() || ! $query->is_main_query() || ! is_archive() ) return;

	// We only want to catch *event* category requests being issued
	// against something other than list view
	if ( ! $query->get( 'tribe_events_cat' ) ) return;
	if ( tribe_is_list_view() ) return;

	// Get the term object
	$term = get_term_by( 'slug', $query->get( 'tribe_events_cat' ), Tribe__Events__Main::TAXONOMY );

	// If it's invalid don't go any further
	if ( ! $term ) return;

	// Get the list-view taxonomy link and redirect to it
	header( 'Location: ' . tribe_get_listview_link( $term->term_id ) );
	exit();
}

// Use list view for category requests by hooking into pre_get_posts for event queries
//add_action( 'tribe_events_pre_get_posts', 'use_list_view_for_categories' );

/**
 *
 * Remove the Event Tools metabox from the Events pages in the backend
 * This eliminates the worry that the client will check these boxes thinking
 * that they will trigger the Featured Event in the sub-footer of the site.
 *
 */
function remove_events_tools_metabox() {
	remove_meta_box( 'tribe_events_event_options', 'tribe_events', 'side' );
}
add_action( 'admin_menu', 'remove_events_tools_metabox', 10, 2 );