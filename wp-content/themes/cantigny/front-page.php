<?php
class Front_Page extends Alloy_Template {

  public function hero() {

    return alloy_get_fields( $this->obj_id, array(
      'hero_heading',
      'hero_button_link',
      'hero_button_label',
      'hero_background_image'
    ) );

  }

  public function events_heading() {

    return get_field('events_heading', $this->obj_id );

  }

  public function events() {

    $featured_events = get_field( 'featured_events', $this->obj_id );

    return get_event_data( $featured_events );

  }

  public function today() {

    return alloy_get_fields( $this->obj_id, array(
      'today_tab_label',
      'today_heading',
      'today_description',
      'today_button_link',
      'today_button_label',
      'today_links'
    ) );

  }

  public function weather() {

    return unserialize( get_option( 'w_conditions' ) );

  }

  public function hours() {

    $fields = alloy_get_fields( $this->obj_id, array(
      'hours_tab_label',
      'hours_heading',
      'hours_description',
      'hours_button_link',
      'hours_button_label',
      'hours_table_heading',
      'hours_col1',
      'hours_col2',
      'hours_col3',
      'hours',
      'hours_after_table'
    ) );

    // Only return the table for today's date.
    if( $fields['hours'] ) {
      $today = strtolower( date('l') );

      foreach( $fields['hours'] as $day_set ) {

        if( $day_set['day'] == $today ) {
          $fields['hours'] = $day_set['rows'];
          break;
        }

      }

    }

    return $fields;

  }

  public function fees() {

    $fields = alloy_get_fields( $this->obj_id, array(
      'fees_tab_label',
      'fees_heading',
      'fees_description',
      'fees_button_link',
      'fees_button_label',
      'fees_table_heading',
      'fees_col1',
      'fees_col2',
      'fees_col3',
      'fees',
      'fees_after_table'
    ) );

    // Only return the table for today's date.
    if( $fields['fees'] ) {
      $today = strtolower( date('l') );

      foreach( $fields['fees'] as $day_set ) {

        if( $day_set['day'] == $today ) {
          $fields['fees'] = $day_set['rows'];
          break;
        }

      }

    }

    return $fields;

  }

  public function location() {

    return alloy_get_fields( $this->obj_id, array(
      'location_tab_label',
      'location_heading',
      'location_description',
      'location_button_link',
      'location_button_label',
      'location_map',
      'locations'
    ) );

  }

  public function c1() {

    return alloy_get_fields( $this->obj_id, array(
      'c1_heading',
      'c1_description',
      'c1_map_label',
      'c1_map_link',
      'c1_park_map',
      'c1_background_image'
    ) );

  }

  public function cg() {

    return alloy_get_fields( $this->obj_id, array(
      'cg_heading',
      'cg_description',
      'cg_button_link',
      'cg_button_label',
      'cg'
    ) );

  }

  public function c2() {

    return alloy_get_fields( $this->obj_id, array(
      'c2_heading',
      'c2_sub_heading',
      'c2_description',
      'c2_button_link',
      'c2_button_label',
      'c2_background_image'
    ) );

  }

}

global $post;
new Front_Page($post->ID);