<?php
Alloy::Fieldset('register', array(
  'group_args' => array(
    'title' => 'Home Options',
    'hide_on_screen' => array('the_content'),
    'location' => array(
      array(
        array(
          'param' => 'page_type',
          'operator' => '==',
          'value' => 'front_page'
        )
      )
    )
  ),
  'fields' => array(

    Alloy::Field( 'tab', 'Hero', 'hero_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Heading', 'hero_heading' ),
    Alloy::Field( 'text', 'Button Link', 'hero_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'hero_button_label' ),
    Alloy::Field( 'image', 'Background Image', 'hero_background_image', array(
      'instructions' => 'Dimensions: 1300x600. Recommended format: JPG.',
      'return_format' => 'url'
    ) ),

    Alloy::Field( 'tab', 'Events', 'events_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Heading', 'events_heading' ),

    Alloy::Field('repeater', 'Featured Events', 'featured_events', array(
      'button_label' => 'Add Event',
      'layout' => 'table',
      'max' => 4,
      'sub_fields' => array(

        Alloy::Field( 'post_object', 'Event', 'event', array(
          'post_type' => 'tribe_events'
        ) ),

      )
    ) ),

    Alloy::Field( 'tab', 'Today', 'today_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Tab Label', 'today_tab_label' ),
    Alloy::Field( 'text', 'Heading', 'today_heading' ),
    Alloy::Field( 'textarea', 'Description', 'today_description' ),
    Alloy::Field( 'text', 'Button Link', 'today_button_link' ),
    Alloy::Field( 'text', 'Button Link', 'today_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'today_button_label' ),

    Alloy::Field('repeater', 'Links', 'today_links', array(
      'button_label' => 'Add Link',
      'layout' => 'block',
      'sub_fields' => array(

        Alloy::Field( 'text', 'Link Class', 'css_class' ),
        Alloy::Field( 'text', 'Link', 'link' ),
        Alloy::Field( 'text', 'Label', 'label' ),

        Alloy::Field( 'image', 'Icon', 'icon', array(
          'instructions' => 'Dimensions: 35x40. Recommended format: PNG.',
          'return_format' => 'url'
        ) ),

        Alloy::Field( 'image', 'Background Image', 'background_image', array(
          'instructions' => 'Dimensions: 510x135. Recommended format: JPG.',
          'return_format' => 'url'
        ) ),

      )
    ) ),

    Alloy::Field( 'tab', 'Hours', 'hours_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Tab Label', 'hours_tab_label' ),
    Alloy::Field( 'text', 'Heading', 'hours_heading' ),
    Alloy::Field( 'textarea', 'Description', 'hours_description' ),
    Alloy::Field( 'text', 'Button Link', 'hours_button_link' ),
    Alloy::Field( 'text', 'Button Link', 'hours_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'hours_button_label' ),
    Alloy::Field( 'text', 'Table Heading', 'hours_table_heading' ),
    Alloy::Field( 'text', 'Table Col 1 Label', 'hours_col1' ),
    Alloy::Field( 'text', 'Table Col 2 Label', 'hours_col2' ),
    Alloy::Field( 'text', 'Table Col 3 Label', 'hours_col3' ),

    Alloy::Field('repeater', 'Hours Table', 'hours', array(
      'button_label' => 'Add Day',
      'layout' => 'block',
      'sub_fields' => array(

        Alloy::Field( 'select', 'Day', 'day', array(
          'choices' => array(
            'sunday'    => 'Sunday',
            'monday'    => 'Monday',
            'tuesday'   => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday'  => 'Thursday',
            'friday'    => 'Friday',
            'saturday'  => 'Saturday',
          )
        ) ),

        Alloy::Field('repeater', 'Table Rows', 'rows', array(
          'button_label' => 'Add Row',
          'layout' => 'table',
          'sub_fields' => array(

            Alloy::Field( 'text', 'Col 1 Text', 'col1' ),
            Alloy::Field( 'text', 'Col 2 Text', 'col2' ),
            Alloy::Field( 'text', 'Col 3 Text', 'col3' ),

          )
        ) ),

      )
    ) ),

    Alloy::Field( 'text', 'After Table Text', 'hours_after_table' ),

    Alloy::Field( 'tab', 'Fees', 'fees_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Tab Label', 'fees_tab_label' ),
    Alloy::Field( 'text', 'Heading', 'fees_heading' ),
    Alloy::Field( 'textarea', 'Description', 'fees_description' ),
    Alloy::Field( 'text', 'Button Link', 'fees_button_link' ),
    Alloy::Field( 'text', 'Button Link', 'fees_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'fees_button_label' ),
    Alloy::Field( 'text', 'Table Heading', 'fees_table_heading' ),
    Alloy::Field( 'text', 'Table Col 1 Label', 'fees_col1' ),
    Alloy::Field( 'text', 'Table Col 2 Label', 'fees_col2' ),
    Alloy::Field( 'text', 'Table Col 3 Label', 'fees_col3' ),

    Alloy::Field('repeater', 'Fees Table', 'fees', array(
      'button_label' => 'Add Day',
      'layout' => 'block',
      'sub_fields' => array(

        Alloy::Field( 'select', 'Day', 'day', array(
          'choices' => array(
            'sunday'    => 'Sunday',
            'monday'    => 'Monday',
            'tuesday'   => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday'  => 'Thursday',
            'friday'    => 'Friday',
            'saturday'  => 'Saturday',
          )
        ) ),

        Alloy::Field('repeater', 'Table Rows', 'rows', array(
          'button_label' => 'Add Row',
          'layout' => 'table',
          'sub_fields' => array(

            Alloy::Field( 'text', 'Col 1 Text', 'col1' ),
            Alloy::Field( 'text', 'Col 2 Text', 'col2' ),
            Alloy::Field( 'text', 'Col 3 Text', 'col3' ),

          )
        ) ),

      )
    ) ),

    Alloy::Field( 'text', 'After Table Text', 'fees_after_table' ),

    Alloy::Field( 'tab', 'Location', 'location_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Tab Label', 'location_tab_label' ),
    Alloy::Field( 'text', 'Heading', 'location_heading' ),
    Alloy::Field( 'textarea', 'Description', 'location_description' ),
    Alloy::Field( 'text', 'Button Link', 'location_button_link' ),
    Alloy::Field( 'text', 'Button Link', 'location_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'location_button_label' ),
    Alloy::Field('repeater', 'Locations', 'locations', array(
      'button_label' => 'Add Location',
      'layout' => 'block',
      'max' => 2,
      'sub_fields' => array(

        Alloy::Field( 'text', 'Heading', 'heading' ),
        Alloy::Field( 'text', 'Address', 'address' ),

      )
    ) ),
    Alloy::Field( 'textarea', 'Google Map Embed', 'location_map' ),

    Alloy::Field( 'tab', 'Callout 1', 'callout1_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Heading', 'c1_heading' ),
    Alloy::Field( 'textarea', 'Description', 'c1_description' ),

    Alloy::Field( 'text', 'Map Label', 'c1_map_label' ),
    Alloy::Field( 'text', 'Map Link', 'c1_map_link' ),

    Alloy::Field( 'image', 'Park Map', 'c1_park_map', array(
      'instructions' => 'Dimensions: 510x245. Recommended format: JPG.',
      'return_format' => 'url'
    ) ),

    Alloy::Field( 'image', 'Background Image', 'c1_background_image', array(
      'instructions' => 'Dimensions: 1300x500. Recommended format: JPG.',
      'return_format' => 'url'
    ) ),

    Alloy::Field( 'tab', 'Content Grid', 'cg_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Heading', 'cg_heading' ),
    Alloy::Field( 'textarea', 'Description', 'cg_description' ),

    Alloy::Field( 'text', 'Button Link', 'cg_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'cg_button_label' ),

    Alloy::Field('repeater', 'Grid Items', 'cg', array(
      'button_label' => 'Add Item',
      'layout' => 'block',
      'max' => 4,
      'sub_fields' => array(

        Alloy::Field( 'text', 'Heading', 'heading' ),
        Alloy::Field( 'textarea', 'Description', 'description' ),
        Alloy::Field( 'text', 'Link', 'link' ),
        Alloy::Field( 'text', 'Link Text', 'link_text' ),

        Alloy::Field( 'image', 'Icon', 'icon', array(
          'instructions' => 'Dimensions: 85x85. Recommended format: PNG.',
          'return_format' => 'url'
        ) ),

      )
    ) ),

    Alloy::Field( 'tab', 'Callout 2', 'callout2_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Sub Heading', 'c2_sub_heading' ),
    Alloy::Field( 'text', 'Heading', 'c2_heading' ),

    Alloy::Field( 'text', 'Button Link', 'c2_button_link' ),
    Alloy::Field( 'text', 'Button Label', 'c2_button_label' ),

    Alloy::Field( 'image', 'Background Image', 'c2_background_image', array(
      'instructions' => 'Dimensions: 1300x500. Recommended format: JPG.',
      'return_format' => 'url'
    ) ),

  )

));