<?php
Alloy::Fieldset('register', array(
  'group_args' => array(
    'title' => 'Sidebar Options',
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'tribe_events'
        )
      ),
    )
  ),
  'fields' => array(

    Alloy::Field('repeater', 'Sidebar Content', 'sidebar_content', array(
      'button_label' => 'Add Block',
      'layout' => 'block',
      'sub_fields' => array(

        Alloy::Field( 'wysiwyg', 'Content', 'content' ),

      )
    ) ),

  )
) );