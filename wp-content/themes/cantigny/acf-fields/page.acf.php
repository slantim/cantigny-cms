<?php
Alloy::Fieldset('register', array(
  'group_args' => array(
    'title' => 'Page Content',
    'hide_on_screen' => array( 'the_content', 'featured_image' ),
    'location' => array(
      array(
        array(
          'param' => 'page_template',
          'operator' => '==',
          'value' => 'default'
        ),
        array(
          'param' => 'page_type',
          'operator' => '!=',
          'value' => 'front_page'
        ),
      ),
    ),
    'menu_order'  =>  0,
    'position' => 'acf_after_title',
  ),
  'fields' => array(

    Alloy::Field( 'tab', 'Content', 'content_tab_options' ),

    Alloy::Field( 'message', 'Creating Content', 'csccmesg', array(
      'message' => 'To create this page you can add different layout blocks by clicking Insert.'
    ) ),

    Alloy::Field('flexible_content', 'Layout Blocks', 'layout_blocks', array(
      'button_label' => 'Insert',
      'layouts' => array(

        Alloy::Layout( 'Heading', 'heading', array(
          'sub_fields' => array(

            Alloy::Field( 'text', 'Heading', 'heading' ),
            Alloy::Field( 'textarea', 'Description', 'description' ),

          )
        ) ),

        Alloy::Layout( 'Section Header', 'section_header', array(
        	'sub_fields'  =>  array(
        		Alloy::Field('text', 'Section Header', 'header' )
	        )
        ) ),

        Alloy::Layout( 'Content', 'content', array(
          'sub_fields' => array(
						Alloy::Field('text', 'Container ID', 'container_id', array(
							'instructions'  =>  'Use this to set the container with a specific ID. Mostly for developer use.'
						) ),
            Alloy::Field( 'wysiwyg', 'Content', 'content' ),

            Alloy::Field( 'image', 'Photo', 'photo', array(
              'instructions' => 'Optional photo to show to the right of content. Recommended dimensions: 300x200',
              'return_format' => 'url'
            ) )

          )
        ) ),

        Alloy::Layout( 'Content List', 'content_list', array(
          'sub_fields' => array(
            Alloy::Field( 'text', 'Heading', 'heading', array(
            	'instructions'  =>  'This header applies an H3 style to it.'
            ) ),
            Alloy::Field( 'text', 'Sub-Heading', 'sub_heading', array(
	            'instructions'  =>  'This header applies an H4 style to it.'
            ) ),
            Alloy::Field( 'textarea', 'Description', 'description' ),
            
            Alloy::Field('repeater', 'List', 'list', array(
              'button_label' => 'Add Row',
              'layout' => 'table',
              'sub_fields' => array(

                Alloy::Field( 'textarea', 'Content', 'content' ),

              )
            ) ),

          )
        ) ),

        Alloy::Layout( 'Photo', 'photo', array(
          'sub_fields' => array(

            Alloy::Field( 'image', 'Photo', 'photo', array(
              'return_format' => 'array'
            ) ),

            Alloy::Field( 'text', 'Caption', 'caption' ),

          )
        ) ),

        Alloy::Layout( 'Photo Carousel', 'photo_carousel', array(
          'sub_fields' => array(

          	Alloy::Field('text', 'Header', 'header' ),

            Alloy::Field('repeater', 'Photos', 'photos', array(
              'button_label' => 'Add Photo',
              'layout' => 'block',
              'sub_fields' => array(

                Alloy::Field( 'image', 'Photo', 'photo', array(
                  'return_format' => 'array'
                ) ),

                Alloy::Field( 'text', 'Caption', 'caption' ),

              )
            ) ),

          )
        ) ),

        Alloy::Layout( 'Accordion', 'accordion', array(
          'sub_fields' => array(

            Alloy::Field('repeater', 'Sections', 'sections', array(
              'button_label' => 'Add Section',
              'layout' => 'block',
              'sub_fields' => array(

                Alloy::Field( 'text', 'Heading', 'heading' ),
                Alloy::Field( 'wysiwyg', 'Description', 'description' ),

              )
            ) ),

          )
        ) ),

        Alloy::Layout( 'Photo Lightbox', 'photo_lightbox', array(
          'sub_fields' => array(

            Alloy::Field( 'text', 'Lightbox ID', 'lightbox_id', array(
              'instructions' => 'Must be unique.',
              'required'  => 1,
            ) ),

            Alloy::Field( 'image', 'Cover Photo', 'cover_photo', array(
              'return_format' => 'array'
            ) ),

            Alloy::Field( 'text', 'Button Label', 'button_label', array(
              'instructions' => 'If you would like to use a button instead of a cover photo enter the button label here.'
            ) ),

            Alloy::Field( 'image', 'Lightbox Photo', 'lightbox_photo', array(
              'return_format' => 'array'
            ) ),

            Alloy::Field( 'text', 'Caption', 'caption' ),

          )
        ) ),

        Alloy::Layout( 'Carousel Lightbox', 'carousel_lightbox', array(
          'sub_fields' => array(

            Alloy::Field( 'text', 'Lightbox ID', 'lightbox_id', array(
              'instructions' => 'Must be unique.',
              'required'  => 1,
            ) ),

            Alloy::Field( 'image', 'Cover Photo', 'cover_photo', array(
              'instructions'  =>  'Make sure images are at 1000x625px',
            	'return_format' => 'array'
            ) ),

            Alloy::Field( 'text', 'Caption', 'caption' ),

            Alloy::Field( 'text', 'Button Label', 'button_label', array(
              'instructions' => 'If you would like to use a button instead of a cover photo enter the button label here.'
            ) ),

            Alloy::Field('repeater', 'Photos', 'photos', array(
              'button_label' => 'Add Photo',
              'layout' => 'block',
              'sub_fields' => array(

                Alloy::Field( 'image', 'Photo', 'photo', array(
                  'return_format' => 'array'
                ) ),

                Alloy::Field( 'text', 'Caption', 'caption' ),

              )
            ) ),

          )
        ) ),

        Alloy::Layout( 'Video Lightbox', 'video_lightbox', array(
          'sub_fields' => array(

            Alloy::Field( 'text', 'Lightbox ID', 'lightbox_id', array(
              'instructions' => 'Must be unique.',
              'required'  => 1,
            ) ),

            Alloy::Field( 'image', 'Cover Photo', 'cover_photo', array(
              'return_format' => 'array'
            ) ),

            Alloy::Field( 'text', 'Button Label', 'button_label', array(
              'instructions' => 'If you would like to use a button instead of a cover photo enter the button label here.'
            ) ),

            Alloy::Field( 'textarea', 'Video Code', 'video_code' ),

            Alloy::Field( 'text', 'Caption', 'caption' ),

          )
        ) ),

        Alloy::Layout( 'Form Accordion', 'form_accordion', array(
	        'sub_fields' => array(

		        Alloy::Field( 'text', 'Form Accordion ID', 'form_accordion_id', array(
			        'instructions' => 'Must be unique. Preferably one unique word.'
		        ) ),

						Alloy::Field( 'text', 'Form Button Text', 'form_accordion_text' ),

						Alloy::Field( 'ninja_forms_field', 'Ninja Form', 'form_accordion_ninja', array(
							'instructions'  =>  'Choose which form you\'d like to display in this block.'
						) ),

	        )
        ) ),

      )
    ) ),

    Alloy::Field( 'tab', 'Sidebar', 'sidebar_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field('repeater', 'Sidebar Content', 'sidebar_content', array(
      'button_label' => 'Add Block',
      'layout' => 'block',
      'sub_fields' => array(

        Alloy::Field( 'wysiwyg', 'Content', 'content' ),

      )
    ) ),

    Alloy::Field( 'tab', 'Hero', 'page_options_tab_options' ),

    Alloy::Field( 'text', 'Heading', 'hero_heading' ),
    Alloy::Field( 'textarea', 'Description', 'hero_description' ),
    Alloy::Field( 'image', 'Background Image', 'hero_background_image', array(
      'instructions' => 'Dimensions: 1300x600. Recommended format: JPG.',
      'return_format' => 'url'
    ) ),
    
    Alloy::Field( 'tab', 'Serach Content', 'search_options_tab_options' ),
    
    Alloy::Field( 'textarea', 'Serach Excerpt', 'search_excerpt' ),
    Alloy::Field( 'image', 'Search Image', 'search_image', array(
	    'instructions' => 'Dimensions: 750x400. Recommended format: JPG.',
	    'return_format' => 'array'
    ) ),
  )
) );