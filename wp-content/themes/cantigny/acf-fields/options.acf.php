<?php
Alloy::Fieldset('register', array(
  'group_args' => array(
    'title' => 'Options',
    'location' => array(
      array(
        array(
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'acf-options'
        )
      )
    )
  ),
  'fields' => array(

    Alloy::Field( 'tab', 'Header', 'header_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'select', 'Stylesheet Selector', 'stylesheet_selector', array(
    	'instructions'  => 'Use this dropdown to alternate the stylesheets. Selecting <b>Default</b> will reset the stylesheet to display the correct one based on the time of year.<br/><br/><b>December 1st - February 28th/29th</b>: Winter<br/><b>March 1st - May 31st</b>: Spring<br/><b>June 1st - August 31st</b>: Summer<br/><b>September 1st - November 30th</b>: Autumn',
	    'choices' => array(
	    	''        =>  'Default',
		    'winter'  => 'Winter',
		    'spring'  =>  'Spring',
		    'summer'  =>  'Summer',
		    'autumn'  =>  'Autumn'
	    ),
	    'default_value' =>  array(
	    	''  =>  'Default'
	    )
    ) ),

    Alloy::Field( 'image', 'Logo', 'header_logo', array(
      'instructions' => 'Dimensions: 320x150. Recommended format: PNG.',
      'return_format' => 'url'
    ) ),

    Alloy::Field('repeater', 'Main Nav', 'header_main_nav', array(
      'button_label' => 'Add Link',
      'layout' => 'table',
      'sub_fields' => array(

        Alloy::Field( 'page_link', 'Link', 'link' ),
        Alloy::Field( 'text', 'Label', 'label' ),
        Alloy::Field( 'true_false', 'Has Dropdown?', 'dropdown' )

      )
    ) ),

    Alloy::Field('repeater', 'Sub Nav', 'header_sub_nav', array(
      'button_label' => 'Add Link',
      'layout' => 'table',
      'sub_fields' => array(

        Alloy::Field( 'page_link', 'Link', 'link' ),
        Alloy::Field( 'text', 'Label', 'label' ),

      )
    ) ),

    Alloy::Field('repeater', 'Dropdowns', 'header_dropdowns', array(
      'button_label' => 'Add Dropdown',
      'instructions' => 'Attach a drop down to a link from Main Nav.',
      'layout' => 'block',
      'sub_fields' => array(

        Alloy::Field( 'text', 'Label', 'label', array(
          'instructions' => 'Enter the label from the Main Nav item that you would like this dropdown to display under.'
        ) ),

        Alloy::Field( 'text', 'Heading', 'heading' ),
        Alloy::Field( 'textarea', 'Description', 'description' ),
        Alloy::Field( 'text', 'Link Text', 'link_text' ),
        Alloy::Field( 'page_link', 'Link', 'link' ),

        Alloy::Field('repeater', 'Links', 'links', array(
          'button_label' => 'Add Link',
          'layout' => 'block',
          'sub_fields' => array(

            Alloy::Field( 'page_link', 'Link', 'link' ),
            Alloy::Field( 'text', 'Label', 'label' ),
            Alloy::Field( 'image', 'Image', 'image', array(
              'instructions' => 'Dimensions: 380x260. Recommended format: JPG.',
              'return_format' => 'url'
            ) ),

          )
        ) ),

      )
    ) ),

    Alloy::Field( 'tab', 'Footer', 'footer_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field('repeater', 'Logos', 'footer_logos', array(
      'button_label' => 'Add Logo',
      'layout' => 'table',
      'sub_fields' => array(

        Alloy::Field( 'image', 'Logo', 'logo', array(
          'instructions' => 'Recommended format: PNG.',
          'return_format' => 'url'
        ) ),

        Alloy::Field( 'text', 'Link', 'link' ),
        
        Alloy::Field( '')

      )
    ) ),

    Alloy::Field( 'text', 'Address', 'footer_address' ),
    Alloy::Field( 'text', 'Address Link', 'footer_address_link' ),
    Alloy::Field( 'text', 'Hours', 'footer_hours' ),
    Alloy::Field( 'text', 'Hours Link', 'footer_hours_link' ),
    Alloy::Field( 'text', 'Phone Number', 'footer_phone' ),
    Alloy::Field( 'text', 'Email Address', 'footer_email' ),

    Alloy::Field( 'wysiwyg', 'Visitor Info', 'footer_visitor_info' ),
    Alloy::Field( 'wysiwyg', 'Support Us', 'footer_support_us' ),

    Alloy::Field('repeater', 'Footer Links', 'footer_links', array(
      'button_label' => 'Add Link',
      'layout' => 'table',
      'sub_fields' => array(

        Alloy::Field( 'page_link', 'Link', 'link' ),
        Alloy::Field( 'text', 'Label', 'label' ),

      )
    ) ),

    Alloy::Field('repeater', 'Sitemap', 'footer_sitemap', array(
      'button_label' => 'Add Link',
      'instructions' => 'This is for the "About Cantigny Park" column.',
      'layout' => 'table',
      'sub_fields' => array(

        Alloy::Field( 'page_link', 'Link', 'link' ),
        Alloy::Field( 'text', 'Label', 'label' ),

      )
    ) ),

    Alloy::Field( 'text', 'Email Signup', 'footer_email_signup' ),
    Alloy::Field( 'text', 'Email Signup Link', 'footer_email_signup_link' ),

    Alloy::Field( 'text', 'Facebook Link', 'footer_link_facebook' ),
    Alloy::Field( 'text', 'Twitter Link', 'footer_link_twitter' ),
    Alloy::Field( 'text', 'Pinterest Link', 'footer_link_pinterest' ),
    Alloy::Field( 'text', 'Instagram Link', 'footer_link_instagram' ),
    Alloy::Field( 'text', 'YouTube Link', 'footer_link_youtube' ),

    Alloy::Field( 'textarea', 'Text Club', 'footer_text_club' ),

    Alloy::Field( 'text', 'Copyright', 'footer_copyright' ),

    Alloy::Field( 'tab', 'Map', 'map_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'text', 'Map API Key', 'map_api_key' ),
    Alloy::Field( 'text', 'Map Latitude', 'map_lat' ),
    Alloy::Field( 'text', 'Map Longitude', 'map_long' ),

    Alloy::Field( 'tab', 'Interactive Map', 'int_map_tab_options', array(
      'placement' => 'left'
    ) ),

    Alloy::Field( 'image', 'Map Image', 'int_map_image', array(
      'return_format' => 'url'
    ) ),

    Alloy::Field( 'image', 'Map Point Icon', 'int_map_point', array(
      'return_format' => 'url'
    ) ),

    Alloy::Field('repeater', 'Map Points', 'map_points', array(
      'button_label' => 'Add Point',
      'layout' => 'block',
      'sub_fields' => array(

      	Alloy::Field('text', 'Unique ID', 'id', array(
      		'instructions'  =>  'Give this block an ID. It must be unique. If it is more than one word, place \'-\' instead of spaces.'
	      ) ),
        Alloy::Field( 'text', 'Top', 'top' ),
        Alloy::Field( 'text', 'Left', 'left' ),
        Alloy::Field( 'text', 'Right', 'right' ),
        Alloy::Field('gallery', 'Gallery', 'gallery', array(
        	'instructions'  =>  'Use <b>Title</b> for the image header, and <b>Caption</b> for the content of the image.<br/><br/>Make sure images are at 1000x625px'
        ) ),

      )
    ) ),

    Alloy::Field( 'tab', 'Events', 'events_tab_options', array(
      'placement' => 'left'
    ) ),

   Alloy::Field( 'text', 'Hero Heading', 'events_hero_heading' ),
   Alloy::Field( 'textarea', 'Hero Description', 'events_hero_description' ),

   Alloy::Field( 'image', 'Hero Background Image', 'events_hero_background', array(
      'instructions' => 'Dimensions: 1265x410. Recommended format: JPG.',
      'return_format' => 'url'
    ) ),

   Alloy::Field( 'text', 'Featured Events Heading', 'featured_events_heading' ),

   Alloy::Field('repeater', 'Featured Events', 'featured_events', array(
      'button_label' => 'Add Event',
      'layout' => 'table',
      'max' => 4,
      'sub_fields' => array(

        Alloy::Field( 'post_object', 'Event', 'event', array(
          'post_type' => 'tribe_events'
        ) ),

      )
    ) ),

  )
));