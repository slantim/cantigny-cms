<?php
Alloy::Fieldset('register', array(
  'group_args' => array(
    'title' => 'Event Category Options',
    'location' => array(
      array(
        array(
          'param' => 'taxonomy',
          'operator' => '==',
          'value' => 'tribe_events_cat'
        )
      ),
    )
  ),
  'fields' => array(

    Alloy::Field( 'image', 'Icon', 'cat_icon', array(
      'instructions' => 'Dimensions: 49x49. Recommended format: PNG.',
      'return_format' => 'url'
    ) ),

    Alloy::Field( 'select', 'Color', 'cat_color', array(
      'choices' => array(
        'event-green' => 'Green',
        'event-turquoise' => 'Turquoise',
        'event-yellow' => 'Yellow',
        'event-orange' => 'Orange',
        'event-blue' => 'Blue',
        'event-deeppurple' => 'Deep Purple',
        'event-darkgreen' => 'Dark Green'
      )
    ) ),

  )
) );