<?php
Alloy::Fieldset('register', array(
	'group_args' => array(
		'title' => 'Program Policy',
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-program-policy'
				)
			)
		)
	),
	'fields' => array(
		Alloy::Field( 'wysiwyg', 'Program Policy Content', 'program_policy_content', array(
			'instructions'  =>  'Please note that updating this text will update the text on every event that is using this program policy.'
		) ),
	)
) );