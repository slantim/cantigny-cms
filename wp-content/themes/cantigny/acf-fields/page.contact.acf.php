<?php
Alloy::Fieldset('register', array(
	'group_args' => array(
		'title' => 'Connect With Us',
		'location' => array(
			array(
				array(
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'default'
				),
				array(
					'param' => 'page',
					'operator' => '==',
					'value' => '530'
				),
			),
		),
		'menu_order'  =>  4,
		'position' => 'normal',
	),
	'fields' => array(

		Alloy::Field('text', 'Sub-Header', 'contact_sub_header', array(
			'instructions'  =>  'If this field is left blank, the default value is \'Connect With Us\'',
		) ),

		Alloy::Field('text', 'Header', 'contact_header', array(
			'instructions'  =>  'If this field is left blank, the default value is \'Stay in the Know\'',
		) ),

		Alloy::Field('wysiwyg', 'Content', 'contact_content' ),

		Alloy::Field('repeater', 'Social Media', 'contact_repeater', array(
				'button_label' => 'Add Social Media',
				'layout' => 'block',
				'min' => 0,
				'max' => 5,
				'sub_fields' => array(
					Alloy::Field('text', 'Social Media', 'social_media'),
					Alloy::Field('url', 'Social Media Link', 'social_media_link')
				)
		))
	)

) );