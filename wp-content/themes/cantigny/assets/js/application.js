
// Carousel.

$(document).on('cycle-initialized', '.block-photo-carousel .photos', function() {

  var $this = $(this),
      caption = $this.closest('.block-photo-carousel').find('.caption'),
      num_slides = $this.find('img').length;

  // Set the total number of slides.
  caption.find('.total').text( num_slides );



});

var photo_carousel = $('.block-photo-carousel .photos').cycle({
  'slides': 'img'
});

photo_carousel.on({

  'cycle-after': function(event) {
    var $this = $(this),
        carousel = $this.closest('.block-photo-carousel'),
        cur_index = $this.data('cycle.opts').currSlide + 1,
        caption = $this.find('.cycle-slide-active').data('caption');

    // Set the current slide index.
    carousel.find('.caption .index').text( cur_index );
    carousel.find('.caption .caption-content').empty().text( caption );

  }

});

$('.block-photo-carousel .photos .slide-nav').on('click', function(e) {

  e.preventDefault();

  var $this = $(this);

  if( $this.hasClass('prev') ) {
    slide_action = 'prev';
  } else {
    slide_action = 'next';
  }

  $this.closest('.photos').cycle(slide_action);

});

// Accordions.
$('.block-accordion .title').on('click', function(e) {

  e.preventDefault();

  var $this = $(this),
      icon = $this.find('i');

  if( icon.hasClass('fa-minus') ) {
    icon.addClass('fa-plus').removeClass('fa-minus');
  } else {
    icon.removeClass('fa-plus').addClass('fa-minus');
  }

  $this.next('.message').toggleClass('display-block');

});

// Form Accordion
var $formBlock = $('.block-form-accordion .form-button');

$formBlock.next('.form-accordion').css('display', 'none' );
$formBlock.on('click', function(e) {

	e.preventDefault();

	var $this = $(this),
      $next = $this.next('.form-accordion');

	$next.slideToggle();

});
// Wrap iframes with a class
$('iframe').wrap('<div class="iframe-wrapper" />');

// Lightbox

$('[data-fancybox]').fancybox({
	loop  : true
});

// Reassemble left and right arrows
$('.fancybox-arrow--right').on('click', function() {
  $.fancybox.getInstance('next');
} );
$('.fancybox-arrow--left').on('click', function() {
  $.fancybox.getInstance('prev');
});

$("[data-fancybox]").fancybox({
	buttons : [
	    'close'
  ]
});

/**
 *  Audio player
 *
 *  @since 1.0.0
 */
$('.audio-trigger').on('click', function(e) {
	e.preventDefault();

	var $link = $(this);
	$link.find('i.fa-stack-1x').toggleClass('fa-volume-up fa-volume-off');
	var $wrap = $link.closest('.cant-audio-player');
	var $player = $wrap.find('.wp-audio-shortcode');
	var $btn = $player.find('.mejs-playpause-button');
	$btn.click();

});

$('.page-content p').each(function() {
	var $this = $(this);
	if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
		$this.remove();
});
function content_carousel_init() {

  // Make sure it isn't already there.
  if( $('.content-slide.cycle-slide').length >= 1 ) {
    return;
  }

  $('.content-carousel .carousel-nav a').removeClass('active');
  $('.content-carousel .carousel-nav a:first-child').addClass('active');

  $('.content-carousel .content-slides').cycle({
    slides: '.content-slide',
    timeout: 0
  });
}

if( $(window).width() >= 768 ) {
  content_carousel_init();
}

$(window).resize(function() {

  if( $(window).width() <= 767 ) {

    $('.content-carousel .carousel-nav a').removeClass('active');
    $('.content-carousel .carousel-nav a:first-child').addClass('active');

    $('.content-carousel .content-slides').cycle('destroy');

    $('.content-slides').removeAttr('style');

  } else {
    content_carousel_init();
  }

});

// Slide nav.
var carousel_trigger = $('.content-carousel .carousel-nav a');

carousel_trigger.on('click', function(e) {
  e.preventDefault();

  carousel_trigger.removeClass('active');

  var $this = $(this),
      slide_index = $this.data('slide');

  $this.addClass('active');

  $('.content-carousel .content-slides').cycle('goto', slide_index);

});

// Mobile slide nav.
$('.content-slide .mobile-slide-label').on('click', function(e) {
  e.preventDefault();

  var $this = $(this);

  if( $this.hasClass('slide-open') ) {

    $this.removeClass('slide-open');
    $this.find('i').addClass('fa-angle-down').removeClass('fa-angle-up');
    $this.next('.content-half').removeClass('display-block').next('.content-half').removeClass('display-block');

  } else {
    $('.content-slide .mobile-slide-label').removeClass('slide-open');
	  $('.content-slide .mobile-slide-label').find('i').addClass('fa-angle-down').removeClass('fa-angle-up');
	  $('.content-slide .mobile-slide-label').next('.content-half').removeClass('display-block').next('.content-half').removeClass('display-block');
    $this.addClass('slide-open');
    $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    $this.next('.content-half').addClass('display-block').next('.content-half').addClass('display-block');

  }

});
$('.event-categories-mobile-trigger').on('click', function(e) {
  e.preventDefault();

  var $this = $(this);

  if( $this.hasClass('category-open') ) {

    $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    $this.removeClass('category-open').next('.category-list').removeClass('display-block');

  } else {

    $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    $this.addClass('category-open').next('.category-list').addClass('display-block');

  }

});
function featured_events_carousel() {

  var window_width = $(window).width(),
      carousel = $('.featured-events .events-list');

  if( window_width <= 767 ) {

    carousel.cycle({
      slides: '.event',
      pager: '.events-pager'
    });

  } else {

    carousel.cycle('destroy');

  }

}

featured_events_carousel();

$(window).resize(function() {
  featured_events_carousel();
});

$('.featured-events .events-list .event-card' ).hover(function() {
  $(this).toggleClass('active');
});
// Mobile footer accordions

function footerAccordion() {
  $( 'footer .top-heading' ).on( 'click', function( e ) {

    e.preventDefault();

    var $this = $( this ),
      panel = $this.next( '.col-content' );

    if ( panel.hasClass( 'display-block' ) ) {

      $this.removeClass( 'panel-open' );
      $this.find( 'i' ).removeClass( 'fa-angle-up' ).addClass( 'fa-angle-down' );
      panel.removeClass( 'display-block' );

    } else {

    	$('footer .top-heading').removeClass('panel-open');
    	$('footer .top-heading').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
	    $('footer .top-heading').next('.col-content').removeClass('display-block');
      $this.addClass( 'panel-open' );
      $this.find( 'i' ).removeClass( 'fa-angle-down' ).addClass( 'fa-angle-up' );
      panel.addClass( 'display-block' );

    }
  })
}

	$(function() {
	  if ($(window).width() < 768 ) {
		  footerAccordion();
	  }
	});

  $(window).resize( function() {
	  if ($(window).width() < 768 ) {
		  footerAccordion()
	  }
  });



// Main nav dropdown.
// $('.has-dropdown').on('click', function(e) {
// $('.has-dropdown').on('click', function(e) {
//
//   e.preventDefault();
//
//   var $this = $(this),
//       dropdown_id = $this.data('dropdown'),
//       dropdown = $('.dropdown-menu[data-dropdown="' + dropdown_id + '"]');
//
//   if( dropdown.hasClass('display-block') ) {
//
//     dropdown.removeClass('display-block');
//     $this.removeClass('dropdown-open');
//
//   } else {
//     $( '.has-dropdown.dropdown-open' ).removeClass('dropdown-open');
//     $( '.dropdown-menu.display-block' ).removeClass( 'display-block' );
//     $('.search-dropdown').removeClass('display-block');
//     dropdown.addClass('display-block');
//
//     $this.addClass('dropdown-open');
//
//   }
//
// });

$('.jPanelMenu-panel').removeClass('transform').addClass('no-transform');

// Mobile menu slideout.
var jPM = $.jPanelMenu(),
    window_width = $(window).width();

if( window_width > 550 ) {
  open_position = '40%';
} else {
  open_position = '75%';
}

var jPM = $.jPanelMenu({
  menu: '.mobile-menu',
  trigger: '.mobile-menu-trigger',
  openPosition: open_position,
  keyboardShortcuts: false,
	beforeOpen : function() {
		$('.jPanelMenu-panel').removeClass('no-transform').removeClass('header-stuck').addClass('transform');
		$('.header-container').removeClass('sticky');
		$('.search-dropdown').removeClass('display-block');
		$('.jPanelMenu-panel > main').addClass('menu-open');
	},
	afterClose : function() {
		$('.jPanelMenu-panel').removeClass('transform').addClass('no-transform');
		$('.search-dropdown').removeClass('display-block');
		$('.mobile-menu .main-has-dropdown').removeClass('dropdown-open');
		$('.mobile-menu .main-has-dropdown i').removeClass('fa-angle-up').addClass('fa-angle-down');
		$('.mobile-menu .main-has-dropdown .dropdown').removeClass('display-block slideInDown');
		$('.jPanelMenu-panel > main').removeClass('menu-open');
	}
});
jPM.on();

$('.mobile-menu-close-trigger').on('click', function() {
	jPM.close();
});

jPM.close();

// Search dropdown.
$('.main-nav .search').on('click', function(e) {
	var $search = $( '.search-dropdown' ),
		$nav    = $( '.has-dropdown.dropdown-open' ),
		$drop   = $( '.dropdown-menu.display-block' );
	e.preventDefault();

	if ( $search.hasClass( 'display-block' ) ) {
		$search.removeClass( 'display-block' );
	} else {
		$nav.removeClass('dropdown-open');
		$drop.removeClass('display-block');
		$search.toggleClass('display-block').find('input').focus();
	}
});

$('#main').on('click', function() {
	$( '.search-dropdown' ).removeClass('display-block');
});

// Mobile dropdown toggle.
$('.mobile-menu .has-dropdown').on('click', function(e) {
  e.preventDefault();

  var $this = $(this),
      dropdown = $this.next('nav.dropdown');

  if( dropdown.hasClass('display-block') ) {
    $this.removeClass('dropdown-open');
    $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    dropdown.removeClass('display-block slideInDown');
  } else {
    $this.addClass('dropdown-open');
    $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    dropdown.addClass('display-block slideInDown');
  }

});
$('.mobile-menu .main-has-dropdown').on('click', function(e) {

	var $this = $(this),
			$dropdown = $this.find('nav.dropdown.links');

	if ( $this.hasClass('dropdown-open') ) {
		$this.removeClass('dropdown-open');
		$this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
		$dropdown.removeClass('display-block slideInDown');
	} else {
		e.preventDefault();

		$('.mobile-menu .main-has-dropdown').removeClass('dropdown-open');
		$('.mobile-menu .main-has-dropdown i').removeClass('fa-angle-up').addClass('fa-angle-down');
		$('.mobile-menu .main-has-dropdown .dropdown').removeClass('display-block slideInDown');

		$this.addClass('dropdown-open');
		$this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
		$dropdown.addClass('display-block slideInDown');
	}

});


// Sticky Nav
// JQUERY VERSION:

( function( $, window, document, undefined ) {
	'use strict';

	var elSelector		= '.header-container',
		elClassNarrow	= 'header-in',
		elNarrowOffset	= 10,
		$element		= $( elSelector );

	if( !$element.length ) return true;

	var elHeight		    = 0,
			elTop			      = 0,
			$document		    = $( document ),
			dHeight			    = 0,
			$window			    = $( window ),
			wHeight			    = 0,
			wScrollCurrent  = 0,
			wScrollBefore	  = 0,
			wScrollDiff		  = 0,
			$body           = $('body');

	$window.on( 'scroll', function() {

		if ( $body.hasClass('logged-in' ) ) {
			if ( $window.width() >= 783 ) {
				elHeight		= $element.outerHeight() - 32;
			}
			else {
				elHeight		= $element.outerHeight() - 46;
			}
		} else {
			elHeight		= $element.outerHeight();
		}

		// elHeight		= $element.outerHeight();
		dHeight			= $document.height();
		wHeight			= $window.height();
		wScrollCurrent	= $window.scrollTop();
		wScrollDiff		= wScrollBefore - wScrollCurrent;
		elTop			= parseInt( $element.css( 'top' ) ) + wScrollDiff;
		var eMain = $('#main');

		// toggles "narrow" classname
		$element.toggleClass( elClassNarrow, wScrollCurrent > elNarrowOffset );
		// scrolled to the very top; element sticks to the top
		if( wScrollCurrent <= 0 ) {
			$element.css( 'top', 0 );
		}
		// scrolled up; element slides in
		else if( wScrollDiff > 0 ) {
			$element.css( 'top', elTop > 0 ? 0 : elTop );
			eMain.addClass('scrolled');
		}
		// scrolled down
		else if( wScrollDiff < 0 ) {
			$('.dropdown-menu').removeClass('display-block');
			$('.has-dropdown').removeClass('dropdown-open');
			$('.search-dropdown').removeClass('display-block');
			// scrolled to the very bottom; element slides in
			if( wScrollCurrent + wHeight >= dHeight - elHeight ) {
				$element.css( 'top', ( elTop = wScrollCurrent + wHeight - dHeight ) < 0 ? elTop : 0 );
				eMain.addClass('scrolled');

			}
			// scrolled down; element slides out
			else {
				$element.css( 'top', Math.abs( elTop ) > elHeight ? -elHeight : elTop );
				eMain.addClass('scrolled');
			}
		}

		wScrollBefore = wScrollCurrent;
	});

	var eMain = $('.jPanelMenu-panel > main');

	elHeight	= $element.outerHeight();

	$(function() {
		eMain.css('top', elHeight + 'px' );
	});

	$window.resize(function() {
		var eMain = $('#main');
		elHeight	= $element.outerHeight();
		eMain.css('top', elHeight + 'px' );
	});

})( jQuery, window, document );
// Mobile page nav accordion
$('.page-nav-mobile-trigger').on('click', function(e) {
  e.preventDefault();

  var $this = $(this);

  if( $this.hasClass('slide-open') ) {

    $this.removeClass('slide-open').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    $this.next('nav').removeClass('display-flex');

  } else {

    $this.addClass('slide-open').find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    $this.next('nav').addClass('display-flex');

  }

});

// Top/second level page accordion
$('.has-sub-nav').on('click', function(e) {

  var $this = $(this),
      $subPage = $('.sub-page-nav');

	if ( $this.next($subPage).hasClass('display-none') ) {
		e.preventDefault();
		if ( !$('.has-sub-nav').hasClass('active') ) {
			$('.has-sub-nav').next($subPage).addClass('display-none');
		}
	  $this.next($subPage).removeClass('display-none');
  }

});

$(function(){
	var $sidebar = $('.sidebar-nav .has-sub-nav.active');

	if ( $sidebar.hasClass('active') )  {
		$sidebar.next('.sub-page-nav').removeClass('display-none');
	}

});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJhc2UuanMiLCJzY3JpcHRzL2NvbnRlbnQtYmxvY2tzLmpzIiwic2NyaXB0cy9jb250ZW50LWNhcm91c2VsLmpzIiwic2NyaXB0cy9ldmVudC1jYXRlZ29yaWVzLmpzIiwic2NyaXB0cy9mZWF0dXJlZC1ldmVudHMuanMiLCJzY3JpcHRzL2Zvb3Rlci5qcyIsInNjcmlwdHMvaGVhZGVyLmpzIiwic2NyaXB0cy9wYWdlLW5hdi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMvSEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDNU5BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYXBwbGljYXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIiLCIvLyBDYXJvdXNlbC5cblxuJChkb2N1bWVudCkub24oJ2N5Y2xlLWluaXRpYWxpemVkJywgJy5ibG9jay1waG90by1jYXJvdXNlbCAucGhvdG9zJywgZnVuY3Rpb24oKSB7XG5cbiAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgIGNhcHRpb24gPSAkdGhpcy5jbG9zZXN0KCcuYmxvY2stcGhvdG8tY2Fyb3VzZWwnKS5maW5kKCcuY2FwdGlvbicpLFxuICAgICAgbnVtX3NsaWRlcyA9ICR0aGlzLmZpbmQoJ2ltZycpLmxlbmd0aDtcblxuICAvLyBTZXQgdGhlIHRvdGFsIG51bWJlciBvZiBzbGlkZXMuXG4gIGNhcHRpb24uZmluZCgnLnRvdGFsJykudGV4dCggbnVtX3NsaWRlcyApO1xuXG5cblxufSk7XG5cbnZhciBwaG90b19jYXJvdXNlbCA9ICQoJy5ibG9jay1waG90by1jYXJvdXNlbCAucGhvdG9zJykuY3ljbGUoe1xuICAnc2xpZGVzJzogJ2ltZydcbn0pO1xuXG5waG90b19jYXJvdXNlbC5vbih7XG5cbiAgJ2N5Y2xlLWFmdGVyJzogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICBjYXJvdXNlbCA9ICR0aGlzLmNsb3Nlc3QoJy5ibG9jay1waG90by1jYXJvdXNlbCcpLFxuICAgICAgICBjdXJfaW5kZXggPSAkdGhpcy5kYXRhKCdjeWNsZS5vcHRzJykuY3VyclNsaWRlICsgMSxcbiAgICAgICAgY2FwdGlvbiA9ICR0aGlzLmZpbmQoJy5jeWNsZS1zbGlkZS1hY3RpdmUnKS5kYXRhKCdjYXB0aW9uJyk7XG5cbiAgICAvLyBTZXQgdGhlIGN1cnJlbnQgc2xpZGUgaW5kZXguXG4gICAgY2Fyb3VzZWwuZmluZCgnLmNhcHRpb24gLmluZGV4JykudGV4dCggY3VyX2luZGV4ICk7XG4gICAgY2Fyb3VzZWwuZmluZCgnLmNhcHRpb24gLmNhcHRpb24tY29udGVudCcpLmVtcHR5KCkudGV4dCggY2FwdGlvbiApO1xuXG4gIH1cblxufSk7XG5cbiQoJy5ibG9jay1waG90by1jYXJvdXNlbCAucGhvdG9zIC5zbGlkZS1uYXYnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG5cbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gIHZhciAkdGhpcyA9ICQodGhpcyk7XG5cbiAgaWYoICR0aGlzLmhhc0NsYXNzKCdwcmV2JykgKSB7XG4gICAgc2xpZGVfYWN0aW9uID0gJ3ByZXYnO1xuICB9IGVsc2Uge1xuICAgIHNsaWRlX2FjdGlvbiA9ICduZXh0JztcbiAgfVxuXG4gICR0aGlzLmNsb3Nlc3QoJy5waG90b3MnKS5jeWNsZShzbGlkZV9hY3Rpb24pO1xuXG59KTtcblxuLy8gQWNjb3JkaW9ucy5cbiQoJy5ibG9jay1hY2NvcmRpb24gLnRpdGxlJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuXG4gIGUucHJldmVudERlZmF1bHQoKTtcblxuICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgaWNvbiA9ICR0aGlzLmZpbmQoJ2knKTtcblxuICBpZiggaWNvbi5oYXNDbGFzcygnZmEtbWludXMnKSApIHtcbiAgICBpY29uLmFkZENsYXNzKCdmYS1wbHVzJykucmVtb3ZlQ2xhc3MoJ2ZhLW1pbnVzJyk7XG4gIH0gZWxzZSB7XG4gICAgaWNvbi5yZW1vdmVDbGFzcygnZmEtcGx1cycpLmFkZENsYXNzKCdmYS1taW51cycpO1xuICB9XG5cbiAgJHRoaXMubmV4dCgnLm1lc3NhZ2UnKS50b2dnbGVDbGFzcygnZGlzcGxheS1ibG9jaycpO1xuXG59KTtcblxuLy8gRm9ybSBBY2NvcmRpb25cbnZhciAkZm9ybUJsb2NrID0gJCgnLmJsb2NrLWZvcm0tYWNjb3JkaW9uIC5mb3JtLWJ1dHRvbicpO1xuXG4kZm9ybUJsb2NrLm5leHQoJy5mb3JtLWFjY29yZGlvbicpLmNzcygnZGlzcGxheScsICdub25lJyApO1xuJGZvcm1CbG9jay5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG5cblx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdHZhciAkdGhpcyA9ICQodGhpcyksXG4gICAgICAkbmV4dCA9ICR0aGlzLm5leHQoJy5mb3JtLWFjY29yZGlvbicpO1xuXG5cdCRuZXh0LnNsaWRlVG9nZ2xlKCk7XG5cbn0pO1xuLy8gV3JhcCBpZnJhbWVzIHdpdGggYSBjbGFzc1xuJCgnaWZyYW1lJykud3JhcCgnPGRpdiBjbGFzcz1cImlmcmFtZS13cmFwcGVyXCIgLz4nKTtcblxuLy8gTGlnaHRib3hcblxuJCgnW2RhdGEtZmFuY3lib3hdJykuZmFuY3lib3goe1xuXHRsb29wICA6IHRydWVcbn0pO1xuXG4vLyBSZWFzc2VtYmxlIGxlZnQgYW5kIHJpZ2h0IGFycm93c1xuJCgnLmZhbmN5Ym94LWFycm93LS1yaWdodCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAkLmZhbmN5Ym94LmdldEluc3RhbmNlKCduZXh0Jyk7XG59ICk7XG4kKCcuZmFuY3lib3gtYXJyb3ctLWxlZnQnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgJC5mYW5jeWJveC5nZXRJbnN0YW5jZSgncHJldicpO1xufSk7XG5cbiQoXCJbZGF0YS1mYW5jeWJveF1cIikuZmFuY3lib3goe1xuXHRidXR0b25zIDogW1xuXHQgICAgJ2Nsb3NlJ1xuICBdXG59KTtcblxuLyoqXG4gKiAgQXVkaW8gcGxheWVyXG4gKlxuICogIEBzaW5jZSAxLjAuMFxuICovXG4kKCcuYXVkaW8tdHJpZ2dlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdHZhciAkbGluayA9ICQodGhpcyk7XG5cdCRsaW5rLmZpbmQoJ2kuZmEtc3RhY2stMXgnKS50b2dnbGVDbGFzcygnZmEtdm9sdW1lLXVwIGZhLXZvbHVtZS1vZmYnKTtcblx0dmFyICR3cmFwID0gJGxpbmsuY2xvc2VzdCgnLmNhbnQtYXVkaW8tcGxheWVyJyk7XG5cdHZhciAkcGxheWVyID0gJHdyYXAuZmluZCgnLndwLWF1ZGlvLXNob3J0Y29kZScpO1xuXHR2YXIgJGJ0biA9ICRwbGF5ZXIuZmluZCgnLm1lanMtcGxheXBhdXNlLWJ1dHRvbicpO1xuXHQkYnRuLmNsaWNrKCk7XG5cbn0pO1xuXG4kKCcucGFnZS1jb250ZW50IHAnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHR2YXIgJHRoaXMgPSAkKHRoaXMpO1xuXHRpZigkdGhpcy5odG1sKCkucmVwbGFjZSgvXFxzfCZuYnNwOy9nLCAnJykubGVuZ3RoID09IDApXG5cdFx0JHRoaXMucmVtb3ZlKCk7XG59KTsiLCJmdW5jdGlvbiBjb250ZW50X2Nhcm91c2VsX2luaXQoKSB7XG5cbiAgLy8gTWFrZSBzdXJlIGl0IGlzbid0IGFscmVhZHkgdGhlcmUuXG4gIGlmKCAkKCcuY29udGVudC1zbGlkZS5jeWNsZS1zbGlkZScpLmxlbmd0aCA+PSAxICkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gICQoJy5jb250ZW50LWNhcm91c2VsIC5jYXJvdXNlbC1uYXYgYScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgJCgnLmNvbnRlbnQtY2Fyb3VzZWwgLmNhcm91c2VsLW5hdiBhOmZpcnN0LWNoaWxkJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXG4gICQoJy5jb250ZW50LWNhcm91c2VsIC5jb250ZW50LXNsaWRlcycpLmN5Y2xlKHtcbiAgICBzbGlkZXM6ICcuY29udGVudC1zbGlkZScsXG4gICAgdGltZW91dDogMFxuICB9KTtcbn1cblxuaWYoICQod2luZG93KS53aWR0aCgpID49IDc2OCApIHtcbiAgY29udGVudF9jYXJvdXNlbF9pbml0KCk7XG59XG5cbiQod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKSB7XG5cbiAgaWYoICQod2luZG93KS53aWR0aCgpIDw9IDc2NyApIHtcblxuICAgICQoJy5jb250ZW50LWNhcm91c2VsIC5jYXJvdXNlbC1uYXYgYScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAkKCcuY29udGVudC1jYXJvdXNlbCAuY2Fyb3VzZWwtbmF2IGE6Zmlyc3QtY2hpbGQnKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cbiAgICAkKCcuY29udGVudC1jYXJvdXNlbCAuY29udGVudC1zbGlkZXMnKS5jeWNsZSgnZGVzdHJveScpO1xuXG4gICAgJCgnLmNvbnRlbnQtc2xpZGVzJykucmVtb3ZlQXR0cignc3R5bGUnKTtcblxuICB9IGVsc2Uge1xuICAgIGNvbnRlbnRfY2Fyb3VzZWxfaW5pdCgpO1xuICB9XG5cbn0pO1xuXG4vLyBTbGlkZSBuYXYuXG52YXIgY2Fyb3VzZWxfdHJpZ2dlciA9ICQoJy5jb250ZW50LWNhcm91c2VsIC5jYXJvdXNlbC1uYXYgYScpO1xuXG5jYXJvdXNlbF90cmlnZ2VyLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gIGNhcm91c2VsX3RyaWdnZXIucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXG4gIHZhciAkdGhpcyA9ICQodGhpcyksXG4gICAgICBzbGlkZV9pbmRleCA9ICR0aGlzLmRhdGEoJ3NsaWRlJyk7XG5cbiAgJHRoaXMuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXG4gICQoJy5jb250ZW50LWNhcm91c2VsIC5jb250ZW50LXNsaWRlcycpLmN5Y2xlKCdnb3RvJywgc2xpZGVfaW5kZXgpO1xuXG59KTtcblxuLy8gTW9iaWxlIHNsaWRlIG5hdi5cbiQoJy5jb250ZW50LXNsaWRlIC5tb2JpbGUtc2xpZGUtbGFiZWwnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gIGUucHJldmVudERlZmF1bHQoKTtcblxuICB2YXIgJHRoaXMgPSAkKHRoaXMpO1xuXG4gIGlmKCAkdGhpcy5oYXNDbGFzcygnc2xpZGUtb3BlbicpICkge1xuXG4gICAgJHRoaXMucmVtb3ZlQ2xhc3MoJ3NsaWRlLW9wZW4nKTtcbiAgICAkdGhpcy5maW5kKCdpJykuYWRkQ2xhc3MoJ2ZhLWFuZ2xlLWRvd24nKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtdXAnKTtcbiAgICAkdGhpcy5uZXh0KCcuY29udGVudC1oYWxmJykucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKS5uZXh0KCcuY29udGVudC1oYWxmJykucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKTtcblxuICB9IGVsc2Uge1xuICAgICQoJy5jb250ZW50LXNsaWRlIC5tb2JpbGUtc2xpZGUtbGFiZWwnKS5yZW1vdmVDbGFzcygnc2xpZGUtb3BlbicpO1xuXHQgICQoJy5jb250ZW50LXNsaWRlIC5tb2JpbGUtc2xpZGUtbGFiZWwnKS5maW5kKCdpJykuYWRkQ2xhc3MoJ2ZhLWFuZ2xlLWRvd24nKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtdXAnKTtcblx0ICAkKCcuY29udGVudC1zbGlkZSAubW9iaWxlLXNsaWRlLWxhYmVsJykubmV4dCgnLmNvbnRlbnQtaGFsZicpLnJlbW92ZUNsYXNzKCdkaXNwbGF5LWJsb2NrJykubmV4dCgnLmNvbnRlbnQtaGFsZicpLnJlbW92ZUNsYXNzKCdkaXNwbGF5LWJsb2NrJyk7XG4gICAgJHRoaXMuYWRkQ2xhc3MoJ3NsaWRlLW9wZW4nKTtcbiAgICAkdGhpcy5maW5kKCdpJykucmVtb3ZlQ2xhc3MoJ2ZhLWFuZ2xlLWRvd24nKS5hZGRDbGFzcygnZmEtYW5nbGUtdXAnKTtcbiAgICAkdGhpcy5uZXh0KCcuY29udGVudC1oYWxmJykuYWRkQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKS5uZXh0KCcuY29udGVudC1oYWxmJykuYWRkQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKTtcblxuICB9XG5cbn0pOyIsIiQoJy5ldmVudC1jYXRlZ29yaWVzLW1vYmlsZS10cmlnZ2VyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICBpZiggJHRoaXMuaGFzQ2xhc3MoJ2NhdGVnb3J5LW9wZW4nKSApIHtcblxuICAgICR0aGlzLmZpbmQoJ2knKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtdXAnKS5hZGRDbGFzcygnZmEtYW5nbGUtZG93bicpO1xuICAgICR0aGlzLnJlbW92ZUNsYXNzKCdjYXRlZ29yeS1vcGVuJykubmV4dCgnLmNhdGVnb3J5LWxpc3QnKS5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jaycpO1xuXG4gIH0gZWxzZSB7XG5cbiAgICAkdGhpcy5maW5kKCdpJykucmVtb3ZlQ2xhc3MoJ2ZhLWFuZ2xlLWRvd24nKS5hZGRDbGFzcygnZmEtYW5nbGUtdXAnKTtcbiAgICAkdGhpcy5hZGRDbGFzcygnY2F0ZWdvcnktb3BlbicpLm5leHQoJy5jYXRlZ29yeS1saXN0JykuYWRkQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKTtcblxuICB9XG5cbn0pOyIsImZ1bmN0aW9uIGZlYXR1cmVkX2V2ZW50c19jYXJvdXNlbCgpIHtcblxuICB2YXIgd2luZG93X3dpZHRoID0gJCh3aW5kb3cpLndpZHRoKCksXG4gICAgICBjYXJvdXNlbCA9ICQoJy5mZWF0dXJlZC1ldmVudHMgLmV2ZW50cy1saXN0Jyk7XG5cbiAgaWYoIHdpbmRvd193aWR0aCA8PSA3NjcgKSB7XG5cbiAgICBjYXJvdXNlbC5jeWNsZSh7XG4gICAgICBzbGlkZXM6ICcuZXZlbnQnLFxuICAgICAgcGFnZXI6ICcuZXZlbnRzLXBhZ2VyJ1xuICAgIH0pO1xuXG4gIH0gZWxzZSB7XG5cbiAgICBjYXJvdXNlbC5jeWNsZSgnZGVzdHJveScpO1xuXG4gIH1cblxufVxuXG5mZWF0dXJlZF9ldmVudHNfY2Fyb3VzZWwoKTtcblxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpIHtcbiAgZmVhdHVyZWRfZXZlbnRzX2Nhcm91c2VsKCk7XG59KTtcblxuJCgnLmZlYXR1cmVkLWV2ZW50cyAuZXZlbnRzLWxpc3QgLmV2ZW50LWNhcmQnICkuaG92ZXIoZnVuY3Rpb24oKSB7XG4gICQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xufSk7IiwiLy8gTW9iaWxlIGZvb3RlciBhY2NvcmRpb25zXG5cbmZ1bmN0aW9uIGZvb3RlckFjY29yZGlvbigpIHtcbiAgJCggJ2Zvb3RlciAudG9wLWhlYWRpbmcnICkub24oICdjbGljaycsIGZ1bmN0aW9uKCBlICkge1xuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgdmFyICR0aGlzID0gJCggdGhpcyApLFxuICAgICAgcGFuZWwgPSAkdGhpcy5uZXh0KCAnLmNvbC1jb250ZW50JyApO1xuXG4gICAgaWYgKCBwYW5lbC5oYXNDbGFzcyggJ2Rpc3BsYXktYmxvY2snICkgKSB7XG5cbiAgICAgICR0aGlzLnJlbW92ZUNsYXNzKCAncGFuZWwtb3BlbicgKTtcbiAgICAgICR0aGlzLmZpbmQoICdpJyApLnJlbW92ZUNsYXNzKCAnZmEtYW5nbGUtdXAnICkuYWRkQ2xhc3MoICdmYS1hbmdsZS1kb3duJyApO1xuICAgICAgcGFuZWwucmVtb3ZlQ2xhc3MoICdkaXNwbGF5LWJsb2NrJyApO1xuXG4gICAgfSBlbHNlIHtcblxuICAgIFx0JCgnZm9vdGVyIC50b3AtaGVhZGluZycpLnJlbW92ZUNsYXNzKCdwYW5lbC1vcGVuJyk7XG4gICAgXHQkKCdmb290ZXIgLnRvcC1oZWFkaW5nJykuZmluZCgnaScpLnJlbW92ZUNsYXNzKCdmYS1hbmdsZS11cCcpLmFkZENsYXNzKCdmYS1hbmdsZS1kb3duJyk7XG5cdCAgICAkKCdmb290ZXIgLnRvcC1oZWFkaW5nJykubmV4dCgnLmNvbC1jb250ZW50JykucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKTtcbiAgICAgICR0aGlzLmFkZENsYXNzKCAncGFuZWwtb3BlbicgKTtcbiAgICAgICR0aGlzLmZpbmQoICdpJyApLnJlbW92ZUNsYXNzKCAnZmEtYW5nbGUtZG93bicgKS5hZGRDbGFzcyggJ2ZhLWFuZ2xlLXVwJyApO1xuICAgICAgcGFuZWwuYWRkQ2xhc3MoICdkaXNwbGF5LWJsb2NrJyApO1xuXG4gICAgfVxuICB9KVxufVxuXG5cdCQoZnVuY3Rpb24oKSB7XG5cdCAgaWYgKCQod2luZG93KS53aWR0aCgpIDwgNzY4ICkge1xuXHRcdCAgZm9vdGVyQWNjb3JkaW9uKCk7XG5cdCAgfVxuXHR9KTtcblxuICAkKHdpbmRvdykucmVzaXplKCBmdW5jdGlvbigpIHtcblx0ICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA3NjggKSB7XG5cdFx0ICBmb290ZXJBY2NvcmRpb24oKVxuXHQgIH1cbiAgfSk7XG5cblxuIiwiLy8gTWFpbiBuYXYgZHJvcGRvd24uXG4vLyAkKCcuaGFzLWRyb3Bkb3duJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuLy8gJCgnLmhhcy1kcm9wZG93bicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbi8vXG4vLyAgIGUucHJldmVudERlZmF1bHQoKTtcbi8vXG4vLyAgIHZhciAkdGhpcyA9ICQodGhpcyksXG4vLyAgICAgICBkcm9wZG93bl9pZCA9ICR0aGlzLmRhdGEoJ2Ryb3Bkb3duJyksXG4vLyAgICAgICBkcm9wZG93biA9ICQoJy5kcm9wZG93bi1tZW51W2RhdGEtZHJvcGRvd249XCInICsgZHJvcGRvd25faWQgKyAnXCJdJyk7XG4vL1xuLy8gICBpZiggZHJvcGRvd24uaGFzQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKSApIHtcbi8vXG4vLyAgICAgZHJvcGRvd24ucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXktYmxvY2snKTtcbi8vICAgICAkdGhpcy5yZW1vdmVDbGFzcygnZHJvcGRvd24tb3BlbicpO1xuLy9cbi8vICAgfSBlbHNlIHtcbi8vICAgICAkKCAnLmhhcy1kcm9wZG93bi5kcm9wZG93bi1vcGVuJyApLnJlbW92ZUNsYXNzKCdkcm9wZG93bi1vcGVuJyk7XG4vLyAgICAgJCggJy5kcm9wZG93bi1tZW51LmRpc3BsYXktYmxvY2snICkucmVtb3ZlQ2xhc3MoICdkaXNwbGF5LWJsb2NrJyApO1xuLy8gICAgICQoJy5zZWFyY2gtZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jaycpO1xuLy8gICAgIGRyb3Bkb3duLmFkZENsYXNzKCdkaXNwbGF5LWJsb2NrJyk7XG4vL1xuLy8gICAgICR0aGlzLmFkZENsYXNzKCdkcm9wZG93bi1vcGVuJyk7XG4vL1xuLy8gICB9XG4vL1xuLy8gfSk7XG5cbiQoJy5qUGFuZWxNZW51LXBhbmVsJykucmVtb3ZlQ2xhc3MoJ3RyYW5zZm9ybScpLmFkZENsYXNzKCduby10cmFuc2Zvcm0nKTtcblxuLy8gTW9iaWxlIG1lbnUgc2xpZGVvdXQuXG52YXIgalBNID0gJC5qUGFuZWxNZW51KCksXG4gICAgd2luZG93X3dpZHRoID0gJCh3aW5kb3cpLndpZHRoKCk7XG5cbmlmKCB3aW5kb3dfd2lkdGggPiA1NTAgKSB7XG4gIG9wZW5fcG9zaXRpb24gPSAnNDAlJztcbn0gZWxzZSB7XG4gIG9wZW5fcG9zaXRpb24gPSAnNzUlJztcbn1cblxudmFyIGpQTSA9ICQualBhbmVsTWVudSh7XG4gIG1lbnU6ICcubW9iaWxlLW1lbnUnLFxuICB0cmlnZ2VyOiAnLm1vYmlsZS1tZW51LXRyaWdnZXInLFxuICBvcGVuUG9zaXRpb246IG9wZW5fcG9zaXRpb24sXG4gIGtleWJvYXJkU2hvcnRjdXRzOiBmYWxzZSxcblx0YmVmb3JlT3BlbiA6IGZ1bmN0aW9uKCkge1xuXHRcdCQoJy5qUGFuZWxNZW51LXBhbmVsJykucmVtb3ZlQ2xhc3MoJ25vLXRyYW5zZm9ybScpLnJlbW92ZUNsYXNzKCdoZWFkZXItc3R1Y2snKS5hZGRDbGFzcygndHJhbnNmb3JtJyk7XG5cdFx0JCgnLmhlYWRlci1jb250YWluZXInKS5yZW1vdmVDbGFzcygnc3RpY2t5Jyk7XG5cdFx0JCgnLnNlYXJjaC1kcm9wZG93bicpLnJlbW92ZUNsYXNzKCdkaXNwbGF5LWJsb2NrJyk7XG5cdFx0JCgnLmpQYW5lbE1lbnUtcGFuZWwgPiBtYWluJykuYWRkQ2xhc3MoJ21lbnUtb3BlbicpO1xuXHR9LFxuXHRhZnRlckNsb3NlIDogZnVuY3Rpb24oKSB7XG5cdFx0JCgnLmpQYW5lbE1lbnUtcGFuZWwnKS5yZW1vdmVDbGFzcygndHJhbnNmb3JtJykuYWRkQ2xhc3MoJ25vLXRyYW5zZm9ybScpO1xuXHRcdCQoJy5zZWFyY2gtZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jaycpO1xuXHRcdCQoJy5tb2JpbGUtbWVudSAubWFpbi1oYXMtZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZHJvcGRvd24tb3BlbicpO1xuXHRcdCQoJy5tb2JpbGUtbWVudSAubWFpbi1oYXMtZHJvcGRvd24gaScpLnJlbW92ZUNsYXNzKCdmYS1hbmdsZS11cCcpLmFkZENsYXNzKCdmYS1hbmdsZS1kb3duJyk7XG5cdFx0JCgnLm1vYmlsZS1tZW51IC5tYWluLWhhcy1kcm9wZG93biAuZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jayBzbGlkZUluRG93bicpO1xuXHRcdCQoJy5qUGFuZWxNZW51LXBhbmVsID4gbWFpbicpLnJlbW92ZUNsYXNzKCdtZW51LW9wZW4nKTtcblx0fVxufSk7XG5qUE0ub24oKTtcblxuJCgnLm1vYmlsZS1tZW51LWNsb3NlLXRyaWdnZXInKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcblx0alBNLmNsb3NlKCk7XG59KTtcblxualBNLmNsb3NlKCk7XG5cbi8vIFNlYXJjaCBkcm9wZG93bi5cbiQoJy5tYWluLW5hdiAuc2VhcmNoJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuXHR2YXIgJHNlYXJjaCA9ICQoICcuc2VhcmNoLWRyb3Bkb3duJyApLFxuXHRcdCRuYXYgICAgPSAkKCAnLmhhcy1kcm9wZG93bi5kcm9wZG93bi1vcGVuJyApLFxuXHRcdCRkcm9wICAgPSAkKCAnLmRyb3Bkb3duLW1lbnUuZGlzcGxheS1ibG9jaycgKTtcblx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdGlmICggJHNlYXJjaC5oYXNDbGFzcyggJ2Rpc3BsYXktYmxvY2snICkgKSB7XG5cdFx0JHNlYXJjaC5yZW1vdmVDbGFzcyggJ2Rpc3BsYXktYmxvY2snICk7XG5cdH0gZWxzZSB7XG5cdFx0JG5hdi5yZW1vdmVDbGFzcygnZHJvcGRvd24tb3BlbicpO1xuXHRcdCRkcm9wLnJlbW92ZUNsYXNzKCdkaXNwbGF5LWJsb2NrJyk7XG5cdFx0JHNlYXJjaC50b2dnbGVDbGFzcygnZGlzcGxheS1ibG9jaycpLmZpbmQoJ2lucHV0JykuZm9jdXMoKTtcblx0fVxufSk7XG5cbiQoJyNtYWluJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG5cdCQoICcuc2VhcmNoLWRyb3Bkb3duJyApLnJlbW92ZUNsYXNzKCdkaXNwbGF5LWJsb2NrJyk7XG59KTtcblxuLy8gTW9iaWxlIGRyb3Bkb3duIHRvZ2dsZS5cbiQoJy5tb2JpbGUtbWVudSAuaGFzLWRyb3Bkb3duJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgIGRyb3Bkb3duID0gJHRoaXMubmV4dCgnbmF2LmRyb3Bkb3duJyk7XG5cbiAgaWYoIGRyb3Bkb3duLmhhc0NsYXNzKCdkaXNwbGF5LWJsb2NrJykgKSB7XG4gICAgJHRoaXMucmVtb3ZlQ2xhc3MoJ2Ryb3Bkb3duLW9wZW4nKTtcbiAgICAkdGhpcy5maW5kKCdpJykucmVtb3ZlQ2xhc3MoJ2ZhLWFuZ2xlLXVwJykuYWRkQ2xhc3MoJ2ZhLWFuZ2xlLWRvd24nKTtcbiAgICBkcm9wZG93bi5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jayBzbGlkZUluRG93bicpO1xuICB9IGVsc2Uge1xuICAgICR0aGlzLmFkZENsYXNzKCdkcm9wZG93bi1vcGVuJyk7XG4gICAgJHRoaXMuZmluZCgnaScpLnJlbW92ZUNsYXNzKCdmYS1hbmdsZS1kb3duJykuYWRkQ2xhc3MoJ2ZhLWFuZ2xlLXVwJyk7XG4gICAgZHJvcGRvd24uYWRkQ2xhc3MoJ2Rpc3BsYXktYmxvY2sgc2xpZGVJbkRvd24nKTtcbiAgfVxuXG59KTtcbiQoJy5tb2JpbGUtbWVudSAubWFpbi1oYXMtZHJvcGRvd24nKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG5cblx0dmFyICR0aGlzID0gJCh0aGlzKSxcblx0XHRcdCRkcm9wZG93biA9ICR0aGlzLmZpbmQoJ25hdi5kcm9wZG93bi5saW5rcycpO1xuXG5cdGlmICggJHRoaXMuaGFzQ2xhc3MoJ2Ryb3Bkb3duLW9wZW4nKSApIHtcblx0XHQkdGhpcy5yZW1vdmVDbGFzcygnZHJvcGRvd24tb3BlbicpO1xuXHRcdCR0aGlzLmZpbmQoJ2knKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtdXAnKS5hZGRDbGFzcygnZmEtYW5nbGUtZG93bicpO1xuXHRcdCRkcm9wZG93bi5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jayBzbGlkZUluRG93bicpO1xuXHR9IGVsc2Uge1xuXHRcdGUucHJldmVudERlZmF1bHQoKTtcblxuXHRcdCQoJy5tb2JpbGUtbWVudSAubWFpbi1oYXMtZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZHJvcGRvd24tb3BlbicpO1xuXHRcdCQoJy5tb2JpbGUtbWVudSAubWFpbi1oYXMtZHJvcGRvd24gaScpLnJlbW92ZUNsYXNzKCdmYS1hbmdsZS11cCcpLmFkZENsYXNzKCdmYS1hbmdsZS1kb3duJyk7XG5cdFx0JCgnLm1vYmlsZS1tZW51IC5tYWluLWhhcy1kcm9wZG93biAuZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jayBzbGlkZUluRG93bicpO1xuXG5cdFx0JHRoaXMuYWRkQ2xhc3MoJ2Ryb3Bkb3duLW9wZW4nKTtcblx0XHQkdGhpcy5maW5kKCdpJykucmVtb3ZlQ2xhc3MoJ2ZhLWFuZ2xlLWRvd24nKS5hZGRDbGFzcygnZmEtYW5nbGUtdXAnKTtcblx0XHQkZHJvcGRvd24uYWRkQ2xhc3MoJ2Rpc3BsYXktYmxvY2sgc2xpZGVJbkRvd24nKTtcblx0fVxuXG59KTtcblxuXG4vLyBTdGlja3kgTmF2XG4vLyBKUVVFUlkgVkVSU0lPTjpcblxuKCBmdW5jdGlvbiggJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkICkge1xuXHQndXNlIHN0cmljdCc7XG5cblx0dmFyIGVsU2VsZWN0b3JcdFx0PSAnLmhlYWRlci1jb250YWluZXInLFxuXHRcdGVsQ2xhc3NOYXJyb3dcdD0gJ2hlYWRlci1pbicsXG5cdFx0ZWxOYXJyb3dPZmZzZXRcdD0gMTAsXG5cdFx0JGVsZW1lbnRcdFx0PSAkKCBlbFNlbGVjdG9yICk7XG5cblx0aWYoICEkZWxlbWVudC5sZW5ndGggKSByZXR1cm4gdHJ1ZTtcblxuXHR2YXIgZWxIZWlnaHRcdFx0ICAgID0gMCxcblx0XHRcdGVsVG9wXHRcdFx0ICAgICAgPSAwLFxuXHRcdFx0JGRvY3VtZW50XHRcdCAgICA9ICQoIGRvY3VtZW50ICksXG5cdFx0XHRkSGVpZ2h0XHRcdFx0ICAgID0gMCxcblx0XHRcdCR3aW5kb3dcdFx0XHQgICAgPSAkKCB3aW5kb3cgKSxcblx0XHRcdHdIZWlnaHRcdFx0XHQgICAgPSAwLFxuXHRcdFx0d1Njcm9sbEN1cnJlbnQgID0gMCxcblx0XHRcdHdTY3JvbGxCZWZvcmVcdCAgPSAwLFxuXHRcdFx0d1Njcm9sbERpZmZcdFx0ICA9IDAsXG5cdFx0XHQkYm9keSAgICAgICAgICAgPSAkKCdib2R5Jyk7XG5cblx0JHdpbmRvdy5vbiggJ3Njcm9sbCcsIGZ1bmN0aW9uKCkge1xuXG5cdFx0aWYgKCAkYm9keS5oYXNDbGFzcygnbG9nZ2VkLWluJyApICkge1xuXHRcdFx0aWYgKCAkd2luZG93LndpZHRoKCkgPj0gNzgzICkge1xuXHRcdFx0XHRlbEhlaWdodFx0XHQ9ICRlbGVtZW50Lm91dGVySGVpZ2h0KCkgLSAzMjtcblx0XHRcdH1cblx0XHRcdGVsc2Uge1xuXHRcdFx0XHRlbEhlaWdodFx0XHQ9ICRlbGVtZW50Lm91dGVySGVpZ2h0KCkgLSA0Njtcblx0XHRcdH1cblx0XHR9IGVsc2Uge1xuXHRcdFx0ZWxIZWlnaHRcdFx0PSAkZWxlbWVudC5vdXRlckhlaWdodCgpO1xuXHRcdH1cblxuXHRcdC8vIGVsSGVpZ2h0XHRcdD0gJGVsZW1lbnQub3V0ZXJIZWlnaHQoKTtcblx0XHRkSGVpZ2h0XHRcdFx0PSAkZG9jdW1lbnQuaGVpZ2h0KCk7XG5cdFx0d0hlaWdodFx0XHRcdD0gJHdpbmRvdy5oZWlnaHQoKTtcblx0XHR3U2Nyb2xsQ3VycmVudFx0PSAkd2luZG93LnNjcm9sbFRvcCgpO1xuXHRcdHdTY3JvbGxEaWZmXHRcdD0gd1Njcm9sbEJlZm9yZSAtIHdTY3JvbGxDdXJyZW50O1xuXHRcdGVsVG9wXHRcdFx0PSBwYXJzZUludCggJGVsZW1lbnQuY3NzKCAndG9wJyApICkgKyB3U2Nyb2xsRGlmZjtcblx0XHR2YXIgZU1haW4gPSAkKCcjbWFpbicpO1xuXG5cdFx0Ly8gdG9nZ2xlcyBcIm5hcnJvd1wiIGNsYXNzbmFtZVxuXHRcdCRlbGVtZW50LnRvZ2dsZUNsYXNzKCBlbENsYXNzTmFycm93LCB3U2Nyb2xsQ3VycmVudCA+IGVsTmFycm93T2Zmc2V0ICk7XG5cdFx0Ly8gc2Nyb2xsZWQgdG8gdGhlIHZlcnkgdG9wOyBlbGVtZW50IHN0aWNrcyB0byB0aGUgdG9wXG5cdFx0aWYoIHdTY3JvbGxDdXJyZW50IDw9IDAgKSB7XG5cdFx0XHQkZWxlbWVudC5jc3MoICd0b3AnLCAwICk7XG5cdFx0fVxuXHRcdC8vIHNjcm9sbGVkIHVwOyBlbGVtZW50IHNsaWRlcyBpblxuXHRcdGVsc2UgaWYoIHdTY3JvbGxEaWZmID4gMCApIHtcblx0XHRcdCRlbGVtZW50LmNzcyggJ3RvcCcsIGVsVG9wID4gMCA/IDAgOiBlbFRvcCApO1xuXHRcdFx0ZU1haW4uYWRkQ2xhc3MoJ3Njcm9sbGVkJyk7XG5cdFx0fVxuXHRcdC8vIHNjcm9sbGVkIGRvd25cblx0XHRlbHNlIGlmKCB3U2Nyb2xsRGlmZiA8IDAgKSB7XG5cdFx0XHQkKCcuZHJvcGRvd24tbWVudScpLnJlbW92ZUNsYXNzKCdkaXNwbGF5LWJsb2NrJyk7XG5cdFx0XHQkKCcuaGFzLWRyb3Bkb3duJykucmVtb3ZlQ2xhc3MoJ2Ryb3Bkb3duLW9wZW4nKTtcblx0XHRcdCQoJy5zZWFyY2gtZHJvcGRvd24nKS5yZW1vdmVDbGFzcygnZGlzcGxheS1ibG9jaycpO1xuXHRcdFx0Ly8gc2Nyb2xsZWQgdG8gdGhlIHZlcnkgYm90dG9tOyBlbGVtZW50IHNsaWRlcyBpblxuXHRcdFx0aWYoIHdTY3JvbGxDdXJyZW50ICsgd0hlaWdodCA+PSBkSGVpZ2h0IC0gZWxIZWlnaHQgKSB7XG5cdFx0XHRcdCRlbGVtZW50LmNzcyggJ3RvcCcsICggZWxUb3AgPSB3U2Nyb2xsQ3VycmVudCArIHdIZWlnaHQgLSBkSGVpZ2h0ICkgPCAwID8gZWxUb3AgOiAwICk7XG5cdFx0XHRcdGVNYWluLmFkZENsYXNzKCdzY3JvbGxlZCcpO1xuXG5cdFx0XHR9XG5cdFx0XHQvLyBzY3JvbGxlZCBkb3duOyBlbGVtZW50IHNsaWRlcyBvdXRcblx0XHRcdGVsc2Uge1xuXHRcdFx0XHQkZWxlbWVudC5jc3MoICd0b3AnLCBNYXRoLmFicyggZWxUb3AgKSA+IGVsSGVpZ2h0ID8gLWVsSGVpZ2h0IDogZWxUb3AgKTtcblx0XHRcdFx0ZU1haW4uYWRkQ2xhc3MoJ3Njcm9sbGVkJyk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0d1Njcm9sbEJlZm9yZSA9IHdTY3JvbGxDdXJyZW50O1xuXHR9KTtcblxuXHR2YXIgZU1haW4gPSAkKCcualBhbmVsTWVudS1wYW5lbCA+IG1haW4nKTtcblxuXHRlbEhlaWdodFx0PSAkZWxlbWVudC5vdXRlckhlaWdodCgpO1xuXG5cdCQoZnVuY3Rpb24oKSB7XG5cdFx0ZU1haW4uY3NzKCd0b3AnLCBlbEhlaWdodCArICdweCcgKTtcblx0fSk7XG5cblx0JHdpbmRvdy5yZXNpemUoZnVuY3Rpb24oKSB7XG5cdFx0dmFyIGVNYWluID0gJCgnI21haW4nKTtcblx0XHRlbEhlaWdodFx0PSAkZWxlbWVudC5vdXRlckhlaWdodCgpO1xuXHRcdGVNYWluLmNzcygndG9wJywgZWxIZWlnaHQgKyAncHgnICk7XG5cdH0pO1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7IiwiLy8gTW9iaWxlIHBhZ2UgbmF2IGFjY29yZGlvblxuJCgnLnBhZ2UtbmF2LW1vYmlsZS10cmlnZ2VyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICBpZiggJHRoaXMuaGFzQ2xhc3MoJ3NsaWRlLW9wZW4nKSApIHtcblxuICAgICR0aGlzLnJlbW92ZUNsYXNzKCdzbGlkZS1vcGVuJykuZmluZCgnaScpLnJlbW92ZUNsYXNzKCdmYS1hbmdsZS11cCcpLmFkZENsYXNzKCdmYS1hbmdsZS1kb3duJyk7XG4gICAgJHRoaXMubmV4dCgnbmF2JykucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXktZmxleCcpO1xuXG4gIH0gZWxzZSB7XG5cbiAgICAkdGhpcy5hZGRDbGFzcygnc2xpZGUtb3BlbicpLmZpbmQoJ2knKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtZG93bicpLmFkZENsYXNzKCdmYS1hbmdsZS11cCcpO1xuICAgICR0aGlzLm5leHQoJ25hdicpLmFkZENsYXNzKCdkaXNwbGF5LWZsZXgnKTtcblxuICB9XG5cbn0pO1xuXG4vLyBUb3Avc2Vjb25kIGxldmVsIHBhZ2UgYWNjb3JkaW9uXG4kKCcuaGFzLXN1Yi1uYXYnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG5cbiAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICRzdWJQYWdlID0gJCgnLnN1Yi1wYWdlLW5hdicpO1xuXG5cdGlmICggJHRoaXMubmV4dCgkc3ViUGFnZSkuaGFzQ2xhc3MoJ2Rpc3BsYXktbm9uZScpICkge1xuXHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRpZiAoICEkKCcuaGFzLXN1Yi1uYXYnKS5oYXNDbGFzcygnYWN0aXZlJykgKSB7XG5cdFx0XHQkKCcuaGFzLXN1Yi1uYXYnKS5uZXh0KCRzdWJQYWdlKS5hZGRDbGFzcygnZGlzcGxheS1ub25lJyk7XG5cdFx0fVxuXHQgICR0aGlzLm5leHQoJHN1YlBhZ2UpLnJlbW92ZUNsYXNzKCdkaXNwbGF5LW5vbmUnKTtcbiAgfVxuXG59KTtcblxuJChmdW5jdGlvbigpe1xuXHR2YXIgJHNpZGViYXIgPSAkKCcuc2lkZWJhci1uYXYgLmhhcy1zdWItbmF2LmFjdGl2ZScpO1xuXG5cdGlmICggJHNpZGViYXIuaGFzQ2xhc3MoJ2FjdGl2ZScpICkgIHtcblx0XHQkc2lkZWJhci5uZXh0KCcuc3ViLXBhZ2UtbmF2JykucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXktbm9uZScpO1xuXHR9XG5cbn0pOyJdfQ==
