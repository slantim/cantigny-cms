<?php
class Page extends Alloy_Template {

  public $top_nav_id;

  public function breadcrumb() {
    return get_breadcrumb( $this->obj_id );
  }

  public function hero() {

    return alloy_get_fields( $this->obj_id, array(
      'hero_heading',
      'hero_description',
      'hero_background_image'
    ) );

  }

  public function top_nav() {

    // Check what level we are on.
    $ancestors = get_post_ancestors( $this->obj_id );

    $top_level_id = end( $ancestors );

    if( !$top_level_id ) {
      return;
    }

    $page_args = array(
      'post_type' => 'page',
      'posts_per_page' => -1,
      'post_parent' => $top_level_id,
      'orderby' => 'menu_order',
      'order' => 'ASC'
    );
    $pages = get_posts( $page_args );

    if( !$pages ) {
      return;
    }

    $nav = array();

    foreach( $pages as $page ) {

      // Check if we are on this page or a descendent of this page.
      if( $this->obj_id == $page->ID || in_array($page->ID, $ancestors ) ) {
        $active = ' active';
        $cur_page_title = $page->post_title;

        $this->top_nav_id = $page->ID;

      } else {
        $active = null;
      }

      $nav[] = array(
        'link' => get_permalink( $page->ID ),
        'label' => $page->post_title,
        'active' => $active
      );

    }

    return array(
      'nav' => $nav,
      'cur_page_title' => $cur_page_title
    );

  }

  public function sub_nav() {

    if( !$this->top_nav_id ) {
      return;
    }

    $ancestors = get_post_ancestors( $this->obj_id );

    // Get the children.
    $page_args = array(
      'post_type' => 'page',
      'posts_per_page' => -1,
      'post_parent' => $this->top_nav_id,
      'orderby' => 'menu_order',
      'order' => 'ASC'
    );
    $pages = get_posts( $page_args );

    if( !$pages ) {
      return;
    }

    $nav = array();

    foreach( $pages as $page ) {

      if( $this->obj_id == $page->ID || in_array($page->ID, $ancestors ) ) {
        $active = ' active';
      } else {
        $active = null;
      }

      // Check to see if there is one more level.
      $sub_page_args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $page->ID
      );
      $sub_pages = get_posts( $sub_page_args );

      if( $sub_pages ) {

        $sub_page_nav = array();

        foreach( $sub_pages as $sub_page ) {

          if( $this->obj_id == $sub_page->ID ) {
            $sub_active = ' active';
          } else {
            $sub_active = null;
          }

          $sub_page_nav[] = array(
            'link' => get_permalink( $sub_page->ID ),
            'label' => $sub_page->post_title,
            'active' => $sub_active
          );

        }

      } else {

        $sub_page_nav = null;

      }

      $nav[] = array(
        'link' => get_permalink( $page->ID ),
        'label' => $page->post_title,
        'active' => $active,
        'sub_page_nav' => $sub_page_nav
      );

    }

    return $nav;

  }

  public function sidebar_blocks() {

    return get_field( 'sidebar_content', $this->obj_id );

  }

  public function blocks() {

    $blocks = get_field( 'layout_blocks', $this->obj_id );

    if( !$blocks ) {
      return;
    }

    $layout_html = '';

    foreach( $blocks as $block ) {

      $layout_html .= Timber::compile( Alloy::Constant( 'theme_dir' ) . '/views/blocks/' . $block['acf_fc_layout'] . '.twig', $block);

      if( $block['acf_fc_layout'] == 'photo_lightbox' ) {

      }

    }

    return array(
      'layout_html' => $layout_html
    );

  }

  public function contact_sm() {

  	$post = get_the_ID();

  	if ( $post != 530 ) {
  		return;
	  }

  	return alloy_get_fields( $post, array(
  		'contact_sub_header',
		  'contact_header',
		  'contact_content',
		  'contact_repeater'
	  ) );

  }

}

global $post;
new Page($post->ID);