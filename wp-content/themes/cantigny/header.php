<?php
class Header extends Alloy_Template {

  public function template_file() {

    return 'views/partials/header.twig';

  }

  public function fields() {

    return alloy_get_fields( 'option', array(
    	'activate_notification_banner',
      'notification_banner_text',
      'header_logo',
      'header_main_nav',
      'header_sub_nav',
      'header_dropdowns'
    ) );

  }

}
new Header;

