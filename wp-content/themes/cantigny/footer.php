<?php
class Footer extends Alloy_Template {

  public function template_file() {

    return 'views/partials/footer.twig';

  }

  public function fields() {

    return alloy_get_fields( 'option', array(
      'footer_logos',
      'footer_address',
      'footer_address_link',
      'footer_hours',
      'footer_hours_link',
      'footer_phone',
      'footer_email',
      'footer_visitor_info',
      'footer_support_us',
      'footer_links',
      'footer_sitemap',
      'footer_email_signup',
      'footer_email_signup_link',
      'footer_link_facebook',
      'footer_link_twitter',
      'footer_link_pinterest',
      'footer_link_instagram',
      'footer_link_youtube',
      'footer_text_club',
      'footer_copyright'
    ) );

  }

}
new Footer;