<?php
class Search extends Alloy_Template {

  public function results() {

    $result_args = array(
      'post_type' => array( 'page', 'tribe_events' ),
      'posts_per_page' => -1,
      'post__not_in' =>  array(
      	//Getting Here
	      364,
      	// Plan Your Visit
	      159,
      	// - Things to Do
	      169,
      	// - Gardens
	      161,
      	// - Museums
	      155,
      	// - Tours & Groups
	      167,
      	// About Cantigny
      	754,
	      // Plan Your Visit
	      159,
	      // Private Events
	      432,
	      // Support Us
	      522,
      ),
      'order' =>  'ASC',
      'orderby' =>  'type',
      's' => get_search_query()
    );

    $results = get_posts ( $result_args );

    if( !$results ) {
      return array(
        'count' => 0
      );
    }

    $result_data = array();

    foreach( $results as $result ) {
	
	    setup_postdata( $result );
	
	    $search_data = alloy_get_fields( $result->ID, array(
		    'hero_background_image',
		    'search_excerpt',
		    'search_image'
	    ) );
	    
	    $layout_blocks = alloy_get_fields( $result->ID, array(
	    	'layout_blocks',
	    ) );
	    
	    // If/Else for Thumbnail
	    if ( has_post_thumbnail( $result->ID ) ) {
		    $thumbnail = get_the_post_thumbnail_url( $result->ID, 'medium' );
	    } elseif ( $search_data['search_image'] != '' ) {
	    	$thumbnail = $search_data['search_image']['sizes']['medium'];
	    } elseif ( $search_data['search_image'] == '' && $search_data['hero_background_image'] != '' ) {
	    	$thumbnail = $search_data['hero_background_image'];
	    } else {
		    $thumbnail = Alloy::Constant( 'theme_url' ) . '/assets/resources/fallback.png';
	    }
	    
	    // If/Else for Content
	    // If the search_excerpt field is filled in
	    if ( $search_data['search_excerpt'] != '' ) {
	    	$content = $search_data['search_excerpt'];
	    }
	    // Elseif there are layout blocks
//	    elseif ( $search_data['search_excerpt'] == '' && $layout_blocks ) {
//		    foreach ( $layout_blocks as $blocks ) {
//		    	foreach ( $blocks as $block ) {
//				    if ( $block['description'] ) {
//					    if ( $block['description'] ) {
//						    $content = $block['description'];
//						    break;
//					    }
//				    } elseif ( $block['content'] ) {
//					    $content = $block['content'];
//					    $content = strip_shortcodes($content);
//					    $content = strip_tags( $content );
//					    $content = wp_trim_words( $content, 30 );
//					    break;
//				    }
//			    }
//		    }
//	    }
	    // Elseif get_the_content
	    elseif ( get_the_excerpt( $result->ID ) != '' ) {
		   $content = wp_trim_words( get_the_content(), 30 );
	    }
	    // Else, there's nothing there
	    else {
	    	$content = '';
	    }
	
	    
	    $result_data[] = array(
		    'thumbnail' => $thumbnail,
		    'title'     => $result->post_title,
		    'content'   => $content,
		    'permalink' => get_permalink( $result->ID )
	    );
	    
	
    }

    wp_reset_postdata();

    return array(
      'count' => count( $results ),
      'data' => $result_data
    );

  }

}
new Search;