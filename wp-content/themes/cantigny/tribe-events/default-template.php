<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class Events extends Alloy_Template {

  public function template_settings() {

    global $post;

    if( is_post_type_archive('tribe_events' ) ) {
      $data['events_page'] = true;
    }
    else {

      $query = get_queried_object();

      if( $query->post_type == 'tribe_events' ) {
        $data['events_single'] = true;
      }

    }

    return $data;

  }

  public function hero() {

    return alloy_get_fields( 'option', array(
      'events_hero_heading',
      'events_hero_description',
      'events_hero_background'
    ) );

  }

  public function featured_events() {

    $fields = alloy_get_fields( 'option', array(
      'featured_events_heading',
      'featured_events'
    ) );

    return array(
      'heading' => $fields['featured_events_heading'],
      'events' => get_event_data( $fields['featured_events'] )
    );

  }

}
global $post;
new Events($post->ID);