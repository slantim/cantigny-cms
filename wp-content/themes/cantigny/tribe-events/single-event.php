<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

$event_data = get_single_event( $event_id );
error_log( print_r( $event_data, true ) );

?>

<div class="gradient-bg">

  <div class="inner">

    <section class="breadcrumb dark page-heading">
      <a href="<?php echo home_url(); ?>">Home</a> <i class="fa fa-chevron-right"></i> <a href="<?php echo home_url('events'); ?>">Programs & Events</a> <i class="fa fa-chevron-right"></i> <a href="<?php echo $event_data['permalink']; ?>"><?php echo $event_data['title']; ?></a>
    </section><!-- / breadcrumb -->

    <section class="page-content has-sidebar">

      <section class="sidebar event-sidebar">

      	<?php
      	if( $event_data['category'] ) {
      		?>
	        <section class="event-category">

	          <a href="<?php echo $event_data['cat_link']; ?>" class="<?php echo $event_data['cat_color']; ?>">
	            <?php
	            if( $event_data['cat_icon'] ) {
	            	echo '<img src="' . $event_data['cat_icon'] . '">';
	            }
	            ?>

	            <?php echo $event_data['category']; ?>
	          </a>

	        </section><!-- / event-category -->
	        <?php
	      }
	      ?>

        <section class="icon-content">

          <div class="icon"><i class="fa fa-calendar"></i></div>

          <section class="description">

						<?php
						if( $event_data['date'] == $event_data['end_date'] ) {
							?>
							<p><strong>Date</strong></p>
							<p><?php echo $event_data['date']; ?></p>
							<?php
						} else {
							?>
							<p><strong>Start Date</strong></p>
							<p><?php echo $event_data['date']; ?></p>

							<p><strong>End Date</strong></p>
							<p><?php echo $event_data['end_date']; ?></p>
							<?php
						}
						?>

          </section><!-- / description -->

        </section><!-- / icon-content -->

				<?php
				if( $event_data['start_time'] && $event_data['end_time'] ) {
					?>
					<section class="icon-content">

						<div class="icon"><i class="fa fa-clock-o"></i></div>

						<section class="description">

							<p><strong>Time</strong></p>
							<p><?php echo $event_data['start_time']; ?> - <?php echo $event_data['end_time']; ?></p>

						</section><!-- / description -->

					</section><!-- / icon-content -->
					<?php
				}

        if( $event_data['venue'] && $event_data['venue']['title'] && $event_data['venue']['address'] ) {
        	?>

	        <section class="icon-content">

	          <div class="icon"><i class="fa fa-map-marker"></i></div>

	          <section class="description">

	            <p><strong>Location</strong></p>
	            <p><?php echo $event_data['venue']['title']; ?></p>
	            <p><?php echo $event_data['venue']['address']; ?></p>
	            <p><?php echo $event_data['venue']['city']; ?>, <?php echo $event_data['venue']['state']; ?> <?php echo $event_data['venue']['zip']; ?></p>

	          </section><!-- / description -->

	        </section><!-- / icon-content -->

	        <?php
	      }

        ?>
				<?php
				$repeatable_fields = get_post_meta($event_id, 'repeatable_fields', true );
				if ( $repeatable_fields != '' ) { ?>
	      	<section class="icon-content">

	          <div class="icon"><i class="fa fa-tag"></i></div>

	          <section class="description">

	            <p><strong>Cost</strong></p>
		          <?php
		          if ( $repeatable_fields != '' ) {
			          foreach ( $repeatable_fields as $field ) :
				          echo '<p>';
				          $label = $field['item_label'];
				          $cost  = $field['add_cost'];
				          $multi = $field['item_multiplier'];
				          if ( $label != '' ) {
					          esc_attr_e( $label );
					          if ( ! empty( $label ) && ! empty( $cost ) ) {
						          echo ' ';
					          }
				          }
				          if ( $cost != '' ) {
				          	if ( strpos( $cost, '$' ) !== false ) {
					          } else {
				          		echo '$';
					          }
					          esc_attr_e( $cost );
					          if ( ! empty( $cost ) && ! empty( $multi ) ) {
						          echo ' ';
					          }
				          }
				          if ( $multi != '' ) {
					          esc_attr_e( $multi );
				          }
				          echo '</p>';
			          endforeach;
		          } ?>


	          </section><!-- / description -->

	        </section><!-- / icon-content -->
	      <?php } ?>
        <?php


	      $sidebar_content = get_field( 'sidebar_content', $event_id );

	      if( $sidebar_content ) {

	      	foreach( $sidebar_content as $block ) {
						echo '<section class="sidebar-content">' . $block['content'] . '</section><!-- / sidebar-content -->';
		      }

	      }
	      ?>
	      
	      <?php
	      $sidebar_program_policy = get_post_meta( $event_id, '_add_program_policy' );
	      
	      if ( $sidebar_program_policy[0] == 1 ) { ?>
					
	      	<section class="sidebar-content">
			      <?php the_field( 'program_policy_content', 'option' ); ?>
		      </section>
	      <?php
	      } ?>
	      
	      

      </section><!-- / sidebar -->

      <section class="content">

        <section class="block block-content">

        	<?php
        	if( has_post_thumbnail( $event_id ) ) {
        		echo '<p><img src="' . get_the_post_thumbnail_url( $event_id, 'full' ) . '"></p>';
        	} else {
		        echo '<p><img src="/wp-content/uploads/2017/09/Event-Placeholder.jpg"></p>';
	        }
        	?>

          <?php while ( have_posts() ) :  the_post(); ?>
          	<?php the_content(); ?>
          <?php endwhile; ?>

        </section><!-- / block-content -->

      </section><!-- / content -->

    </section><!-- / page-content -->

  </div><!-- / inner -->

  <?php
  // Get related events.
  $event_args = array(
  	'post_type' => 'tribe_events',
  	'posts_per_page' => 4,
  	'post__not_in' => array( $event_id ),
  	'tax_query' => array(
  		array(
  			'taxonomy' => 'tribe_events_cat',
  			'field' => 'slug',
  			'terms' => $event_data['cat_slug']
  		)
  	)
  );
  $related_events = get_posts( $event_args );

  if( $related_events ) {
  	?>

	  <section class="featured-events events-page">

	    <div class="inner">

	      <h2>You May Also Like This...</h2>

	      <section class="events-list">

	      	<?php
	      	foreach( $related_events as $event ) {
	      		$d = get_single_event( $event->ID, $event );
	      		?>
	      		<a href="<?php echo $d['permalink']; ?>" class="event">

	      			<?php
	      			if( $d['thumbnail'] ) {
	      				?>
			          <div class="thumbnail" style="background-image: url(<?php echo $d['thumbnail']; ?>);">
			            <img src="<?php echo get_template_directory_uri(); ?>/assets/resources/placeholder-260x135.png">
			          </div>
			          <?php
			        } else { ?>
					      <div class="thumbnail" style="background-image: url(/wp-content/uploads/2017/09/Event-Placeholder.jpg);">
						      <img src="<?php echo get_template_directory_uri(); ?>/assets/resources/placeholder-260x135.png">
					      </div>
				      <?php
					      }
			        ?>

		          <div class="event-card">

		            <span class="date"><?php echo $d['date']; ?></span>
		            <?php
		            if( $d['category'] ) {
		            	echo '<span class="event-category ' . $d['cat_color'] . '">' . $d['category'] . '</span>';
		            }
		            ?>
		            <span class="title"><?php echo $d['title']; ?></span>

		            <span class="button green learn-more">Learn More</span>

		          </div><!-- / event-card -->

		        </a><!-- / event -->
		        <?php
	      	}
	      	?>

	      </section><!-- / events-list -->

	      <section class="events-pager"></section>

	    </div><!-- / inner -->

	  </section><!-- / featured-events -->
	  <?php
	}
	?>

</div><!-- / gradient-bg -->