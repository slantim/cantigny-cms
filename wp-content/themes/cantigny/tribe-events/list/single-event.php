<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @version 4.5.6
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$event_data = get_single_event( get_the_ID() );
?>

<section class="event-list">

    <div class="inner">
			<article class="event-preview">

				<?php
				if( $event_data['thumbnail'] ) {
					?>
				  <a href="<?php echo $event_data['permalink']; ?>" class="thumbnail" style="background-image: url(<?php echo $event_data['thumbnail']; ?>);">
				    <img src="<?php echo get_template_directory_uri(); ?>/assets/resources/placeholder-485x325.png">
				  </a><!-- / thumbnail -->
				  <?php
				} else {
					?>
					<a href="<?php echo $event_data['permalink']; ?>" class="thumbnail" style="background-image: url( /wp-content/uploads/2017/09/Event-Placeholder.jpg );">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/resources/placeholder-485x325.png">
					</a><!-- / thumbnail -->
				<?php } ?>

			  <section class="event-summary">

			    <span class="date"><?php echo $event_data['date']; ?></span>

			    <?php
			    if( $event_data['category'] ) {
			    	?>
			    	<span class="event-category<?php echo ' ' . $event_data['cat_color']; ?>"><?php echo $event_data['category']; ?></span>
			    	<?php
			    }
			    ?>

			    <h3><a href="<?php echo $event_data['permalink']; ?>"><?php echo $event_data['title']; ?></a></h3>
			    <p><?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?></p>
			    <a href="<?php echo $event_data['permalink']; ?>" class="learn-more button green">Learn More</a>

			  </section><!-- / event-summary -->

			</article><!-- / event-preview -->
		</div>
	</section>