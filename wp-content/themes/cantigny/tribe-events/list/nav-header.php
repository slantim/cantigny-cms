<?php
/**
 * List View Nav Template
 * This file loads the list view navigation.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/nav.php
 *
 * @package TribeEventsCalendar
 * @version 4.2
 *
 */

global $wp_query;

$events_label_plural = tribe_get_event_label_plural();

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

	<h3 class="screen-reader-text" tabindex="0"><?php esc_html_e( 'Calendar List Navigation', 'the-events-calendar' ); ?></h3>
	<ul class="tribe-events-sub-nav">

		<li class="event-nav-item tribe-events-nav-previous" aria-label="previous events link">
			<?php tribe_events_the_previous_month_link(); ?>
		</li>
		<li>
			<?php do_action( 'tribe_events_before_the_title' ); ?>
			<h2 class="tribe-events page-title"><?php tribe_events_title(); ?></h2>
			<?php do_action( 'tribe_events_after_the_title' ); ?>
		</li>
		<!-- .tribe-events-nav-previous -->
		<li class="event-nav-item tribe-events-nav-next" aria-label="next events link">
			<?php tribe_events_the_next_month_link(); ?>
		</li>
		<!-- .tribe-events-nav-next -->
	</ul><!-- .tribe-events-sub-nav -->

<script type="application/javascript">
	(function($) {
		$( function() {
			var $bar = $( '.tribe-events-sub-nav' );
			if ( $bar.is( ':empty' ) ) {
				$bar.append( '<li><h2 class="tribe-events page-title"><?php tribe_events_title(); ?></h2></li>' );
			}
		} );
	})(jQuery);
</script>