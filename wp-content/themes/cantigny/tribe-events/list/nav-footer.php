<?php
/**
 * List View Nav Template
 * This file loads the list view navigation.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/nav.php
 *
 * @package TribeEventsCalendar
 * @version 4.2
 *
 */
global $wp_query;

$events_label_plural = tribe_get_event_label_plural();

$prev_month_link  = tribe_get_previous_month_link();
$next_month_link  = tribe_get_next_month_link();

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<h3 class="screen-reader-text" tabindex="0"><?php echo esc_html( sprintf( esc_html__( '%s List Navigation', 'the-events-calendar' ), $events_label_plural ) ); ?></h3>
<ul class="tribe-events-sub-nav">
	<!-- Left Navigation -->
	<li class="event-nav-item tribe-events-nav-previous" aria-label="previous events link">
		<a href="<?php echo esc_url( $prev_month_link ); ?>" rel="prev">
			<span class="fa fa-stack fa-lg" aria-hidden="true">
				<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
				<i class="fa fa-long-arrow-left fa-stack-1x fa-inverse" aria-hidden="true"></i>
			</span>
			<span class="month-name">Previous Page</span>
		</a>
	</li>
	<!-- .tribe-events-nav-previous -->
	<li class="event-nav-item tribe-events-nav-next" aria-label="next events link">
		<a href="<?php echo esc_url( $next_month_link ); ?>" rel="next">
			<span class="month-name">Next Page</span>
			<span class="fa fa-stack fa-lg" aria-hidden="true">
				<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
				<i class="fa fa-long-arrow-right fa-stack-1x fa-inverse" aria-hidden="true"></i>
			</span>
		</a>
	</li>
</ul>
