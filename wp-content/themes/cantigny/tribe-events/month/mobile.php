<?php
/**
 * Please see single-event.php in this directory for detailed instructions on how to use and modify these templates.
 *
 * Override this template in your own theme by creating a file at:
 *
 *     [your-theme]/tribe-events/month/mobile.php
 *
 * @version  4.3.5
 */
?>

<script type="text/html" id="tribe_tmpl_month_mobile_day_header">
	<div class="tribe-mobile-day" data-day="[[=date]]">[[ if(has_events) { ]]
		<h3 class="tribe-mobile-day-heading">[[=i18n.for_date]] <span>[[=raw date_name]]</span></h3>[[ } ]]
	</div>
</script>

<script type="text/html" id="tribe_tmpl_month_mobile">
	<div class="popdown-container tribe-events-mobile tribe-clearfix tribe-events-mobile-event-[[=eventId]][[ if(categoryClasses.length) { ]] [[= categoryClasses]][[ } ]]">

		<div class="popdown-image">
		[[ if (imageSrc.length) { ]]
			<img src="[[=imageSrc]]" alt="[[=title]]">
		[[ } else { ]]
			<img src="[[=missingImg]]" alt="[[=title]]">
		[[ } ]]
		</div>
		<div class="popdown-content">
			<div class="popdown-title">
				<h4 class="popdown-title">
					<a href="[[=permalink]]" title="[[=title]]" rel="bookmark">[[=raw title]]</a>
				</h4>
			</div>
			<div class="popdown-date">
				<p>[[=raw startDate]] at [[=rawnewStartTime]]</p>
			</div>
			[[ if(category.length) { ]]
			<div class="popdown-category [[=raw cat_color]]"><p>[[=raw category]]</p></div>
			[[ } ]]
			[[ if(newExcerpt.length) { ]]
			<div class="popdown-excerpt">
				<p>[[=raw newExcerpt]]</p>
			</div>
			[[ } ]]
			<div class="popdown-learn-more">
				<a href="[[=permalink]]" class="button green compact">Learn More</a>
			</div>
		</div>
	</div>
</script>
