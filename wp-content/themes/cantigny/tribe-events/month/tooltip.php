<?php
/**
 * Please see single-event.php in this directory for detailed instructions on how to use and modify these templates.
 *
 * Override this template in your own theme by creating a file at:
 *
 *     [your-theme]/tribe-events/month/tooltip.php
 * @version 4.4
 */
?>

<script type="text/html" id="tribe_tmpl_tooltip">
	<div id="tribe-events-tooltip-[[=eventId]]" class="tribe-events-tooltip">
		<a href="[[=permalink]]" title="[[title]]">
			<div class="tooltip-flex">
				<div class="tooltip-left">
					[[ if(imageSrc.length) { ]]
					<div class="tooltip-image"><img src="[[=imageSrc]]" /></div>
					[[ } else { ]]
					<div class="tooltip-image"><img src="/wp-content/uploads/2017/10/Event-Placeholder-tooltip.jpg" /></div>
					[[ } ]]
				</div> <!-- /.tooltip-left -->

				<div class="tooltip-right">
					<div class="tooltip-date"><p>[[=raw startDate]] at [[=raw newStartTime]]</p></div>
					[[ if(category.length) { ]]
					<div class="tooltip-category [[=raw cat_color]]"><p>[[=raw category]]</p></div>
					[[ } ]]
					<div class="tooltip-header"><h4>[[=raw title]]</h4></div>
					[[ if(newExcerpt.length) {]]
						<div class="tooltip-content"><p>[[=raw newExcerpt]]</p></div>
					[[ } ]]
					<div class="tooltip-learn-more"><p class="button green compact">Learn More</p></div>
				</div> <!-- /.tooltip-right -->
			</div> <!-- /.tooltip-flex -->
		</a>
	</div>
</script>