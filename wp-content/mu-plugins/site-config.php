<?php
/**
 *  Plugin Name: Master Configurations
 *  Description: Must-have functions and settings for the site
 *  Version: 1.0.0
 *  License: GPL
 *  Author: Nick Makris
 *  
 *  Cantigny must-use functions
 *  
 *  @package Cantigny
 *  @subpackage Admin
 *  @since 1.0.0
 */

/**
 *
 * Include the TinyMCE Shortcode file
 *
 */

require plugin_dir_path(__FILE__) . '/common/tinymce-shortcode/init.php';


/**
 * Removes Adjacent post links from header
 */
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );