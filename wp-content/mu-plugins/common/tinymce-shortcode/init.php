<?php
/**
 * Created by PhpStorm.
 * User: nickmakris
 * Date: 2/17/17
 * Time: 10:47 AM
 */
// init process for registering our button
add_action('init', 'cant_shortcode_button_init');
function cant_shortcode_button_init() {
  
  //Abort early if the user will never see TinyMCE
  if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
    return;
  
  //Add a callback to regiser our tinymce plugin
  add_filter("mce_external_plugins", "cant_register_tinymce_plugin");
  
  // Add a callback to add our button to the TinyMCE toolbar
  add_filter('mce_buttons', 'cant_add_tinymce_button');
}


//This callback registers our plug-in
function cant_register_tinymce_plugin($plugin_array) {
  $plugin_array['cant_download_btn'] = '/wp-content/mu-plugins/common/tinymce-shortcode/js/shortcode.js';
  $plugin_array['cant_button'] = '/wp-content/mu-plugins/common/tinymce-shortcode/js/shortcode.js';
  $plugin_array['cant_map_btn'] = '/wp-content/mu-plugins/common/tinymce-shortcode/js/shortcode.js';
  return $plugin_array;
}

//This callback adds our button to the toolbar
function cant_add_tinymce_button($buttons) {
  //Add the button ID to the $button array
  $buttons[] = "cant_download_btn";
  $buttons[] = "cant_button";
  $buttons[] = "cant_map_btn";
  return $buttons;
}