jQuery(document).ready(function($) {

  tinymce.create('tinymce.plugins.cant_shortcode_plugin', {
    init : function(ed, url) {
      // Register command for when button is clicked
      ed.addCommand('cant_insert_download_btn', function() {
        selected = tinyMCE.activeEditor.selection.getContent();
        
        if( selected ){
          //If text is selected when button is clicked
          //Wrap shortcode around it.
          content =  '[download url=""]'+selected+'[/download]';
        }else{
          content =  '[download url=""][/download]';
        }
        
        tinymce.execCommand('mceInsertContent', false, content);
      });
      ed.addCommand('cant_insert_btn', function() {
        selected = tinyMCE.activeEditor.selection.getContent();
    
        if( selected ){
          //If text is selected when button is clicked
          //Wrap shortcode around it.
          content =  '[button url="" new_window=""]' + selected + '[/button]';
        }else{
          content =  '[button url="" new_window=""][/button]';
        }
    
        tinymce.execCommand('mceInsertContent', false, content);
      });
      ed.addCommand('cant_insert_map_btn', function() {
        selected = tinyMCE.activeEditor.selection.getContent();
    
        if( selected ){
          //If text is selected when button is clicked
          //Wrap shortcode around it.
          content =  '[interactive_map]';
        }else{
          content =  '[interactive_map]';
        }
    
        tinymce.execCommand('mceInsertContent', false, content);
      });
      // Register buttons - trigger above command when clicked
      ed.addButton('cant_download_btn', {title : 'Download Button', cmd : 'cant_insert_download_btn', image: url + '/img/download.png' });
      ed.addButton('cant_button', {title : 'Insert Button', cmd : 'cant_insert_btn', image: url + '/img/btn.png' });
      ed.addButton('cant_map_btn', {title : 'Insert Map', cmd : 'cant_insert_map_btn', image: url + '/img/map.png' });
    },
  });
  
  // Register our TinyMCE plugin
  // first parameter is the button ID1
  // second parameter must match the first parameter of the tinymce.create() function above
  tinymce.PluginManager.add('cant_download_btn', tinymce.plugins.cant_shortcode_plugin);
  tinymce.PluginManager.add('cant_insert_btn', tinymce.plugins.cant_shortcode_plugin);
  tinymce.PluginManager.add('cant_map_btn', tinymce.plugins.cant_shortcode_plugin);
});